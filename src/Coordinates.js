'use strict';
/* global CanvasEngine */



/**
 * Модель координат
 * @param {number} x Координата X
 * @param {number} y Координата Y
 * @constructor
 */
CanvasEngine.Coordinates = function(x, y) {
    /**
     * Координата X
     * @type {number}
     * @private
     */
    this.x_ = x;

    /**
     * Координата Y
     * @type {number}
     * @private
     */
    this.y_ = y;
};


/**
 * Получаем координату X
 * @return {number}
 */
CanvasEngine.Coordinates.prototype.getX = function() {
    return this.x_;
};


/**
 * Получаем координату Y
 * @return {number}
 */
CanvasEngine.Coordinates.prototype.getY = function() {
    return this.y_;
};


/**
 * Устанавливаем координату X
 * @param {number} x
 */
CanvasEngine.Coordinates.prototype.setX = function(x) {
    this.x_ = x;
};


/**
 * Устанавливаем координату Y
 * @param {number} y
 */
CanvasEngine.Coordinates.prototype.setY = function(y) {
    this.y_ = y;
};


/**
 * Увеличиваем координату X на величину val
 * @param {number} val
 */
CanvasEngine.Coordinates.prototype.incX = function(val) {
    this.x_ += val;
};


/**
 * Увеличиваем координату Y на величину val
 * @param {number} val
 */
CanvasEngine.Coordinates.prototype.incY = function(val) {
    this.y_ += val;
};


/**
 * Устанавливаем сразу координату X и Y
 * @param {number} x
 * @param {number} y
 */
CanvasEngine.Coordinates.prototype.setXY = function(x, y) {
    this.x_ = x;
    this.y_ = y;
};


/**
 *
 * @param {CanvasEngine.Coordinates} coordinates
 */
CanvasEngine.Coordinates.prototype.set = function(coordinates) {
    this.x_ = coordinates.getX();
    this.y_ = coordinates.getY();
};


/**
 *
 * @return {CanvasEngine.Coordinates}
 */
CanvasEngine.Coordinates.prototype.clone = function() {
    return new CanvasEngine.Coordinates(this.x_, this.y_);
};
