'use strict';
/* global CanvasEngine */



/**
 * @param {number} x1
 * @param {number} x2
 * @param {number} y1
 * @param {number} y2
 * @constructor
 */
CanvasEngine.IntersectObject = function(x1, x2, y1, y2) {
    /**
     * @type {number}
     * @private
     */
    this.x1_ = x1;


    /**
     * @type {number}
     * @private
     */
    this.x2_ = x2;

    /**
     * @type {number}
     * @private
     */
    this.y1_ = y1;

    /**
     * @type {number}
     * @private
     */
    this.y2_ = y2;

    /**
     *
     * @type {boolean}
     * @private
     */
    this.enable_ = true;

    /**
     *
     * @type {?CanvasEngine.IntersectResult}
     * @private
     */
    this.intersectResult_ = null;
};


/**
 *
 * @return {number}
 */
CanvasEngine.IntersectObject.prototype.getX1 = function() {
    return this.x1_;
};


/**
 *
 * @return {number}
 */
CanvasEngine.IntersectObject.prototype.getX2 = function() {
    return this.x2_;
};


/**
 *
 * @return {number}
 */
CanvasEngine.IntersectObject.prototype.getY1 = function() {
    return this.y1_;
};


/**
 *
 * @param {?CanvasEngine.IntersectResult} intersectResult
 */
CanvasEngine.IntersectObject.prototype.setIntersectResult = function(intersectResult) {
    this.intersectResult_ = intersectResult;
};


/**
 *
 * @return {?CanvasEngine.IntersectResult}
 */
CanvasEngine.IntersectObject.prototype.getIntersectResult = function() {
    return this.intersectResult_;
};


/**
 *
 * @return {number}
 */
CanvasEngine.IntersectObject.prototype.getY2 = function() {
    return this.y2_;
};


/**
 *
 * @param {boolean} enable
 */
CanvasEngine.IntersectObject.prototype.setEnable = function(enable) {
    this.enable_ = enable;
};


/**
 *
 * @return {boolean}
 */
CanvasEngine.IntersectObject.prototype.isEnable = function() {
    return this.enable_;
};


/**
 *
 * @param {CanvasEngine.Coordinates} point
 * @return {boolean}
 */
CanvasEngine.IntersectObject.prototype.isPointInObject = function(point) {
    return this.x1_ <= point.getX() && point.getX() <= this.x2_ && this.y1_ <= point.getY() && point.getY() <= this.y2_;
};

