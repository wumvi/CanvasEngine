'use strict';
/* global CanvasEngine, goog */



/**
 *
 * @param {string} name
 * @param {Object} data
 * @constructor
 */
CanvasEngine.ResLoadEvent = function(name, data) {
  /**
   *
   * @type {string}
   * @private
   */
  this.name_ = name;

  /**
   *
   * @type {Object}
   * @private
   */
  this.data_ = data;
};


/**
 *
 * @return {Object}
 */
CanvasEngine.ResLoadEvent.prototype.getData = function() {
  return this.data_;
};


/**
 *
 * @return {string}
 */
CanvasEngine.ResLoadEvent.prototype.getName = function() {
  return this.name_;
};

/*jshint ignore:start */
// window['CanvasEngine']['ResLoadEvent'] = CanvasEngine.ResLoadEvent;
// CanvasEngine.ResLoadEvent.prototype['getName'] = CanvasEngine.ResLoadEvent.prototype.getName;
// CanvasEngine.ResLoadEvent.prototype['getData'] = CanvasEngine.ResLoadEvent.prototype.getData;
/*jshint ignore:end */
