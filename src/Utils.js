'use strict';

/* global CanvasEngine */

(function() {
  var lastTime = 0;
  var vendors = ['ms', 'moz', 'webkit', 'o'];
  for (var x = 0; x < vendors.length && !window.requestAnimationFrame; x += 1) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] ||
      window[vendors[x] + 'CancelRequestAnimationFrame'];
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function(callback) {
      var currTime = new Date().getTime();
      var timeToCall = Math.max(0, 16 - (currTime - lastTime));
      var id = window.setTimeout(function() {
          this.requestAnimationFrame.timerCurrent += ((new Date().getTime()) - currTime);
          callback(this.requestAnimationFrame.timerCurrent);
        },
        timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }

  window.requestAnimationFrame.timerCurrent = 0;

  window.requestAnimationFrameNew = window.requestAnimationFrame;

  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function(id) {
      clearTimeout(id);
    };
  }

  /*jshint ignore:start */
  window['requestAnimationFrame'] = window.requestAnimationFrame;
  window['requestAnimationFrameNew'] = window.requestAnimationFrameNew;
  window['cancelAnimationFrame'] = window.cancelAnimationFrame;
  /*jshint ignore:end */
}());


/**
 * Подерживается ли touch screen
 * @return {boolean} true - если да
 */
CanvasEngine.isTouchDevice = function() {
  try {
    document.createEvent('TouchEvent');
    return true;
  } catch (e) {
    return false;
  }
};


/**
 *
 * @const {number}
 */
CanvasEngine.DEVICE_PIXEL_RATIO_100 = 1;
//CanvasEngine.DEVICE_PIXEL_RATIO_150 = 1.5;


/**
 *
 * @const {number}
 */
CanvasEngine.DEVICE_PIXEL_RATIO_200 = 2;


/**
 *
 * @const {number}
 */
CanvasEngine.DEVICE_PIXEL_RATIO_300 = 3;


/**
 *
 * @const {number}
 */
CanvasEngine.DEVICE_PIXEL_RATIO_400 = 4;


/**
 *
 * @return {number}
 */
CanvasEngine.getPixelRation = function() {
  var pixelRatio = window['devicePixelRatio'] || 1;
  if (pixelRatio > 4) {
    return 4;
  }

  if ([1, /*1.5,*/ 2, 3, 4].indexOf(pixelRatio) !== -1) {
    return pixelRatio;
  }

  return Math.round(pixelRatio);
};


/**
 *
 * @param {number} pixelRatio
 * @return {string}
 */
CanvasEngine.getPixelRationPrefix = function(pixelRatio) {
  return pixelRatio + 'x';
};

/**
 *
 * @param {Function} cb Callback
 */
CanvasEngine.onScreenRotate = function(cb) {
  var supportsOrientationChange = 'onorientationchange' in window,
      orientationEvent = supportsOrientationChange ? 'orientationchange' : 'resize';
  window.addEventListener(orientationEvent, cb, false);
};


/*jshint ignore:start */
// CanvasEngine['isIOs'] = CanvasEngine.isIOs;
// CanvasEngine['isNativeAndroidBrowser'] = CanvasEngine.isNativeAndroidBrowser;
// CanvasEngine['isAndroidBrowser'] = CanvasEngine.isAndroidBrowser;
// CanvasEngine['getPixelRationPrefix'] = CanvasEngine.getPixelRationPrefix;
// CanvasEngine['getPixelRation'] = CanvasEngine.getPixelRation;
// CanvasEngine['isTouchDevice'] = CanvasEngine.isTouchDevice;
// window['CanvasEngine']['DEVICE_PIXEL_RATIO_100'] = CanvasEngine.DEVICE_PIXEL_RATIO_100;
// window['CanvasEngine']['DEVICE_PIXEL_RATIO_200'] = CanvasEngine.DEVICE_PIXEL_RATIO_200;
// window['CanvasEngine']['DEVICE_PIXEL_RATIO_300'] = CanvasEngine.DEVICE_PIXEL_RATIO_300;
// window['CanvasEngine']['DEVICE_PIXEL_RATIO_400'] = CanvasEngine.DEVICE_PIXEL_RATIO_400;
/*jshint ignore:end */
