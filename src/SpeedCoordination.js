'use strict';
/* global CanvasEngine, goog */



/**
 *
 * @param {number} speed Скорость в пикселах за секунду
 * @param {CanvasEngine.GameCanvas} gameCanvas
 * @constructor
 */
CanvasEngine.SpeedCoordination = function(speed, gameCanvas) {

  /**
   * @private
   * @type {number}
   */
  this.speedValue_ = speed;
  this.setSpeedPerSecond(this.speedValue_);

  /**
   *
   * @type {boolean}
   * @private
   */
  this.isPause_ = false;

  var that = this;

  /**
   *
   * @type {?CanvasEngine.GameCanvas}
   * @private
   */
  this.gameCanvas_ = gameCanvas;
  this.gameCanvas_.addReUpdateFpsNotifyCallback(function() {
    that.onReUpdateFpsChangeStatus_();
  });
};


/**
 *
 * @private
 */
CanvasEngine.SpeedCoordination.prototype.onReUpdateFpsChangeStatus_ = function() {
  this.setSpeedPerSecond(this.speedValue_);
};


/**
 *
 */
CanvasEngine.SpeedCoordination.prototype.pause = function() {
  this.isPause_ = true;
};


/**
 *
 * @return {number}
 */
CanvasEngine.SpeedCoordination.prototype.getFullSpeed = function() {
  return this.speedValue_;
};


/**
 *
 * @return {boolean}
 */
CanvasEngine.SpeedCoordination.prototype.isPause = function() {
  return this.isPause_;
};


/**
 *
 */
CanvasEngine.SpeedCoordination.prototype.resume = function() {
  this.isPause_ = false;
};


/**
 * @deprecated Выкидываем, как не нужный
 * @param {number} timestamp
 */
//CanvasEngine.SpeedCoordination.prototype.setTimeStamp = function(timestamp) {
//this.timeStamp_ = timestamp > 1000 ? timestamp - Math.floor(timestamp/1000) * 1000 : timestamp;
//this.timeStamp_ = timestamp;
//console.log(this.timeStamp_);
//};


/**
 *
 * @param {number} speed
 */
CanvasEngine.SpeedCoordination.prototype.setSpeedPerSecond = function(speed) {
  var that = this;
  setTimeout(function() {
    if (CanvasEngine.GameCanvas.TICKS_PER_SECOND < 30) {
      that.speedPerSecond_ = speed / CanvasEngine.GameCanvas.TICKS_PER_SECOND;
    }
  }, 2000);
  this.speedPerSecond_ = speed / CanvasEngine.GameCanvas.TICKS_PER_SECOND;
};


/**
 *
 * @return {number}
 */
CanvasEngine.SpeedCoordination.prototype.getSpeed = function() {
  return this.gameCanvas_.isTick() && !this.isPause_ ? this.speedPerSecond_ : 0;
};
