'use strict';
/* global CanvasEngine */


/**
 *
 * @param {CanvasEngine.Coordinates} p1
 * @param {CanvasEngine.Coordinates} p2
 * @constructor
 */
CanvasEngine.Line = function (p1, p2) {
    /**
     *
     * @type {CanvasEngine.Coordinates}
     * @private
     */
    this.p1_ = p1;

    /**
     *
     * @type {CanvasEngine.Coordinates}
     * @private
     */
    this.p2_ = p2;
};


/**
 *
 * @return {CanvasEngine.Coordinates}
 */
CanvasEngine.Line.prototype.getP1 = function () {
    return this.p1_;
};


/**
 *
 * @return {CanvasEngine.Coordinates}
 */
CanvasEngine.Line.prototype.getP2 = function () {
    return this.p2_;
};
