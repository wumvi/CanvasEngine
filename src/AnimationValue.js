'use strict';
/* global CanvasEngine, goog */



/**
 * Класс для созданий различных анимаций
 *
 * @param {number} begin Начальная велична
 * @param {number} end Конечная величина
 * @param {number} speed Скорость роста
 * @param {CanvasEngine.GameCanvas} gameCanvas
 * @constructor
 */
CanvasEngine.AnimationValue = function(begin, end, speed, gameCanvas) {
    this.speed_ = new CanvasEngine.SpeedCoordination(speed, gameCanvas);
    this.isPause_ = true;

    this.setInfo(begin, end);
};


/**
 *
 * @param {number} begin Начальная велична
 * @param {number} end Конечная величина
 */
CanvasEngine.AnimationValue.prototype.setInfo = function(begin, end) {
    this.begin_ = begin;
    this.end_ = end;
    this.value_ = this.begin_;

    this.direction_ = begin < end ? 1 : -1;
};


/**
 * Запуск анимации
 */
CanvasEngine.AnimationValue.prototype.start = function() {
    this.isPause_ = false;
    this.reset();
};


/**
 *
 * @param {number} value
 */
CanvasEngine.AnimationValue.prototype.setEndValue = function(value) {
    if (this.isEnd()) {
        this.value_ = value;
    }

    this.end_ = value;
};


/**
 *
 */
CanvasEngine.AnimationValue.prototype.reset = function() {
    this.value_ = this.begin_;
};


/**
 * Стоит режим паузы?
 * @return {boolean} Режим паузы?
 */
CanvasEngine.AnimationValue.prototype.isPause = function() {
    return this.isPause_;
};


/**
 *
 * @param {number} direction
 */
CanvasEngine.AnimationValue.prototype.setDirection = function(direction) {
    this.direction_ = direction;
};


/**
 *
 * @return {boolean}
 */
CanvasEngine.AnimationValue.prototype.isEnd = function() {
    return this.value_ === this.end_ && this.isPause_;
};


/**
 * Получаем значение
 * @return {number} Значение
 */
CanvasEngine.AnimationValue.prototype.getValue = function() {
    if (this.isPause_) {
        return this.value_;
    }

    this.value_ += this.direction_ * this.speed_.getSpeed();
    if (this.direction_ === -1 && this.value_ < this.end_) {
        this.value_ = this.end_;
        this.isPause_ = true;
        return this.value_;
    } else if (this.direction_ === 1 && this.value_ > this.end_) {
        this.value_ = this.end_;
        this.isPause_ = true;
        return this.value_;
    }

    return this.value_;
};
