'use strict';
/* global CanvasEngine */



/**
 *
 * @param {number} width Width of canvas
 * @param {number} height Height of canvas
 * @constructor
 */
CanvasEngine.GameCanvas = function(width, height) {
    /**
     * List of sprite's object
     * @type {Array.<CanvasEngine.SpriteInterface>}
     * @private
     */
    this.surfacesList_ = [];

    /**
     * Dom canvas
     * @type {HTMLCanvasElement}
     * @private
     */
    this.htmlCanvas_ = /** @type {HTMLCanvasElement} */ (document.createElement('canvas'));

    /**
     * Context for Canvas
     * @type {CanvasRenderingContext2D}
     * @private
     */
    this.ctx_ = /** @type {CanvasRenderingContext2D} */ (this.htmlCanvas_.getContext('2d'));

    /**
     * Width of canvas
     * @type {number}
     * @private
     */
    this.widthCanvas_ = 0;

    /**
     * Height of canvas
     * @type {number}
     * @private
     */
    this.heightCanvas_ = 0;

    this.resize(width, height);

    /**
     *
     * @type {boolean}
     * @private
     */
    this.isTick_ = false;

    /**
     *
     * @type {number}
     * @private
     */
    this.tickTimestamp_ = 0;

    /**
     *
     * @type {number}
     * @private
     */
    this.fpsTimestamp_ = 0;

    /**
     *
     * @type {number}
     * @private
     */
    this.fpsValue_ = 0;

    /**
     *
     * @type {Array.<Function>}
     * @private
     */
    this.tickFuncCallback_ = [];

    /**
     *
     * @type {Array.<Function>}
     * @private
     */
    this.reUpdateFpsCallbakList_ = [];

    this.init_();
};


/**
 *
 * @param {number} width
 * @param {number} height
 */
CanvasEngine.GameCanvas.prototype.resize = function(width, height) {
    this.widthCanvas_ = width;
    this.heightCanvas_ = height;

    this.htmlCanvas_.width = this.widthCanvas_;
    this.htmlCanvas_.height = this.heightCanvas_;
};


/**
 * Init variables
 * @private
 */
CanvasEngine.GameCanvas.prototype.init_ = function() {
};


/**
 * Init variables
 */
CanvasEngine.GameCanvas.prototype.reset = function() {
    this.surfacesList_ = [];
    this.reUpdateFpsCallbakList_ = [];
    this.tickFuncCallback_ = [];
};


/**
 * Сортируем спрайты по ZIndex полю, выставляя приоритеты отрисовки
 */
CanvasEngine.GameCanvas.prototype.sortByZIndex = function() {
    var that = this;
    this.surfacesList_.sort(function(item1, item2) {
        return that.sortByZIndexCallback(item1, item2);
    });
};


/**
 *
 * @param {CanvasEngine.SpriteInterface} item1
 * @param {CanvasEngine.SpriteInterface} item2
 * @return {number}
 */
CanvasEngine.GameCanvas.prototype.sortByZIndexCallback = function(item1, item2) {
    return item1.getZIndex() - item2.getZIndex();
};


/**
 * Add sprite to list
 * @param {CanvasEngine.SpriteInterface} sprite
 */
CanvasEngine.GameCanvas.prototype.addSurface = function(sprite) {
    if (sprite instanceof CanvasEngine.Sprite) {
        this.surfacesList_.push(sprite);
    } else {
        throw new Error('sprite must be a SpriteInterface');
    }
};


/**
 *
 * @return {Array.<CanvasEngine.SpriteInterface>}
 */
CanvasEngine.GameCanvas.prototype.getSurfaceList = function() {
    return this.surfacesList_;
};


/**
 *
 * @param {number} offsetX
 * @param {number} offsetY
 */
CanvasEngine.GameCanvas.prototype.onCameraMove = function(offsetX, offsetY) {
    this.ctx_.translate(offsetX, offsetY);
};


/**
 *
 * @return {number}
 */
CanvasEngine.GameCanvas.prototype.getWidth = function() {
    return this.widthCanvas_;
};


/**
 *
 * @return {number}
 */
CanvasEngine.GameCanvas.prototype.getHeight = function() {
    return this.heightCanvas_;
};


/**
 *
 * @return {CanvasRenderingContext2D}
 */
CanvasEngine.GameCanvas.prototype.getContext = function() {
    return this.ctx_;
};


/**
 *
 * @type {number}
 */
CanvasEngine.GameCanvas.TICKS_PER_SECOND = 30;


/**
 *
 * @type {number}
 */
CanvasEngine.GameCanvas.skip_ticks = 1000 / CanvasEngine.GameCanvas.TICKS_PER_SECOND >> 0;


/**
 *
 * @return {boolean}
 */
CanvasEngine.GameCanvas.prototype.isTick = function() {
    return this.isTick_;
};


/**
 *
 * @param {Function} func
 */
CanvasEngine.GameCanvas.prototype.addTickCallback = function(func) {
    this.tickFuncCallback_.push(func);
};


/**
 * Repaint all surface
 * @param {number} timestamp
 */
CanvasEngine.GameCanvas.prototype.repaint = function(timestamp) {
    this.fpsValue_ += 1;
    if (this.fpsTimestamp_ <= timestamp) {
        this.fpsTimestamp_ = timestamp + 1000;
        CanvasEngine.GameCanvas.TICKS_PER_SECOND = this.fpsValue_;// >= 30 ? this.fpsValue_ : 30;
        this.fpsValue_ = 0;
        CanvasEngine.GameCanvas.skip_ticks = 1000 / CanvasEngine.GameCanvas.TICKS_PER_SECOND >> 0;
    }

    this.isTick_ = false;
    if (this.tickTimestamp_ < timestamp) {
        this.tickTimestamp_ += CanvasEngine.GameCanvas.skip_ticks;
        this.isTick_ = true;
        for (var i = 0; i < this.tickFuncCallback_.length; i += 1) {
            this.tickFuncCallback_[i](timestamp);
        }
    }

    this.ctx_.clearRect(0, 0, this.widthCanvas_, this.heightCanvas_);

    for (var num = 0; num < this.surfacesList_.length; num += 1) {
        if (!this.surfacesList_[num].isDraw()) {
            continue;
        }

        this.surfacesList_[num].draw(this.ctx_, timestamp);
    }
};


/**
 *
 * @return {HTMLCanvasElement}
 */
CanvasEngine.GameCanvas.prototype.getCanvas = function() {
    return this.htmlCanvas_;
};


/**
 *
 */
CanvasEngine.GameCanvas.prototype.reUpdateFps = function() {
    for (var num = 0; num < this.reUpdateFpsCallbakList_.length; num += 1) {
        this.reUpdateFpsCallbakList_[num]();
    }
};


/**
 *
 * @param {Function} cb
 */
CanvasEngine.GameCanvas.prototype.addReUpdateFpsNotifyCallback = function(cb) {
    this.reUpdateFpsCallbakList_.push(cb);
};
