'use strict';
/* global CanvasEngine, goog */



/**
 * Модель для события мыши
 * @param {Object} event
 * @param {HTMLElement=} opt_canvasDomElement
 * @constructor
 */
CanvasEngine.ClickEventBridge = function(event, opt_canvasDomElement) {
  /*jshint ignore:start */
  var mainCanvasOffset = jQuery(opt_canvasDomElement).offset();
  if (mainCanvasOffset) {
    this.mouseX_ = (event['originalEvent']['touches'][0]['pageX'] - mainCanvasOffset['left']);
    this.mouseY_ = (event['originalEvent']['touches'][0]['pageY'] - mainCanvasOffset['top']);
  } else {
    this.mouseX_ = event['offsetX'];
    this.mouseY_ = event['offsetY'];
  }
  /*jshint ignore:end */
};


/**
 * Получаем позицию X
 * @return {number}
 */
CanvasEngine.ClickEventBridge.prototype.getX = function() {
  return this.mouseX_;
};


/**
 * Получаем позицию Y
 * @return {number}
 */
CanvasEngine.ClickEventBridge.prototype.getY = function() {
  return this.mouseY_;
};

/*jshint ignore:start */
// window['CanvasEngine']['ClickEventBridge'] = CanvasEngine.ClickEventBridge;
// CanvasEngine.ClickEventBridge.prototype['getX'] = CanvasEngine.ClickEventBridge.prototype.getX;
// CanvasEngine.ClickEventBridge.prototype['getY'] = CanvasEngine.ClickEventBridge.prototype.getY;
/*jshint ignore:end */
