'use strict';
/* global CanvasEngine, goog */



/**
 *
 * @param {string} name
 * @param {Object|CanvasEngine.ResLoadTypeJson} data
 * @constructor
 */
CanvasEngine.ResLoadType = function(name, data) {
  if (typeof name !== 'string') {
    throw new Error('CanvasEngine.ResLoadType name must be string');
  }

  /**
   * @private
   * @type {string}
   */
  this.name_ = name;

  /**
   * @private
   * @type {Object|CanvasEngine.ResLoadTypeJson}
   */
  this.data_ = data;
};


/**
 *
 * @return {Object|CanvasEngine.ResLoadTypeJson}
 */
CanvasEngine.ResLoadType.prototype.getData = function() {
  return this.data_;
};


/**
 *
 * @return {string}
 */
CanvasEngine.ResLoadType.prototype.getName = function() {
  return this.name_;
};


/*jshint ignore:start */
// window['CanvasEngine']['ResLoadType'] = CanvasEngine.ResLoadType;
// CanvasEngine.ResLoadType.prototype['getName'] = CanvasEngine.ResLoadType.prototype.getName;
// CanvasEngine.ResLoadType.prototype['getData'] = CanvasEngine.ResLoadType.prototype.getData;
/*jshint ignore:end */
