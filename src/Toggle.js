'use strict';
/* global CanvasEngine, goog */



/**
 * Класс для созданий различных анимаций
 * @param {number} fadeIn Скорость появления
 * @param {number} fadeOut Скорость исчезания
 * @param {Function} cb
 * @param {CanvasEngine.GameCanvas} gameCanvas
 * @constructor
 */
CanvasEngine.Toggle = function(fadeIn, fadeOut, cb, gameCanvas) {
    /*this.fadeIn_ = fadeIn;
     this.fadeOut_ = fadeOut;

     this.cb_ = cb;
     **/

    gameCanvas.addTickCallback(this.onTickCall.bind(this));
};


/**
 *
 */
CanvasEngine.Toggle.prototype.onTickCall = function() {
    console.log('sdfsd');
};


/**
 *
 * @param {number} begin Начальная велична
 * @param {number} end Конечная величина
 */
CanvasEngine.Toggle.prototype.setInfo = function(begin, end) {
    this.begin_ = begin;
    this.end_ = end;
    this.value_ = this.begin_;

    this.direction_ = begin < end ? 1 : -1;
};


/**
 * Запуск анимации
 */
CanvasEngine.Toggle.prototype.start = function() {
    this.isPause_ = false;
    this.reset();
};


/**
 *
 * @param {number} value
 */
CanvasEngine.Toggle.prototype.setEndValue = function(value) {
    if (this.isEnd()) {
        this.value_ = value;
    }

    this.end_ = value;
};


/**
 *
 */
CanvasEngine.Toggle.prototype.reset = function() {
    this.value_ = this.begin_;
};


/**
 * Стоит режим паузы?
 * @return {boolean} Режим паузы?
 */
CanvasEngine.Toggle.prototype.isPause = function() {
    return this.isPause_;
};


/**
 *
 * @param {number} direction
 */
CanvasEngine.Toggle.prototype.setDirection = function(direction) {
    this.direction_ = direction;
};


/**
 *
 * @return {boolean}
 */
CanvasEngine.Toggle.prototype.isEnd = function() {
    return this.value_ === this.end_ && this.isPause_;
};


/**
 * Получаем значение
 * @return {number} Значение
 */
CanvasEngine.Toggle.prototype.getValue = function() {
   /* if (this.isPause_) {
        return this.value_;
    }

    this.value_ += this.direction_ * this.speed_.getSpeed();
    if (this.direction_ === -1 && this.value_ < this.end_) {
        this.value_ = this.end_;
        this.isPause_ = true;
        return this.value_;
    } else if (this.direction_ === 1 && this.value_ > this.end_) {
        this.value_ = this.end_;
        this.isPause_ = true;
        return this.value_;
    }
    */
    return this.value_;
};
