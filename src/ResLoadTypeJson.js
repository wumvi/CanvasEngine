'use strict';
/* global CanvasEngine, goog */



/**
 *
 * @param {string} url
 * @constructor
 */
CanvasEngine.ResLoadTypeJson = function(url) {
  /**
   *
   * @type {string}
   * @private
   */
  this.url_ = url;
};


/**
 *
 * @return {string}
 */
CanvasEngine.ResLoadTypeJson.prototype.getUrl = function() {
  return this.url_;
};


/*jshint ignore:start */
// window['CanvasEngine']['ResLoadTypeJson'] = CanvasEngine.ResLoadTypeJson;
// CanvasEngine.ResLoadTypeJson.prototype['getUrl'] = CanvasEngine.ResLoadTypeJson.prototype.getUrl;
/*jshint ignore:end */
