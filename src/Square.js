'use strict';
/* global CanvasEngine, goog */



/**
 *
 * @param {number} x
 * @param {number} y
 * @param {number} width
 * @param {number} height
 * @constructor
 */
CanvasEngine.Square = function(x, y, width, height) {
  /**
   *
   * @type {number}
   */
  this.x = x;

  /**
   *
   * @type {number}
   */
  this.y = y;

  /**
   *
   * @type {number}
   * @private
   */
  this.width_ = width;

  /**
   *
   * @type {number}
   * @private
   */
  this.height_ = height;
};


/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getWidth = function() {
  return this.width_;
};


/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getHeight = function() {
  return this.height_;
};


/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getX = function() {
  return this.x;
};


/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getY = function() {
  return this.y;
};


/**
 *
 * @param {number} x
 */
CanvasEngine.Square.prototype.setX = function(x) {
  this.x = x;
};


/**
 *
 * @param {number} y
 */
CanvasEngine.Square.prototype.setY = function(y) {
  this.y = y;
};



/*jshint ignore:start */
// window['CanvasEngine']['Square'] = CanvasEngine.Square;
// CanvasEngine.Square.prototype['getX'] = CanvasEngine.Square.prototype.getX;
// CanvasEngine.Square.prototype['getY'] = CanvasEngine.Square.prototype.getY;
// CanvasEngine.Square.prototype['setX'] = CanvasEngine.Square.prototype.setX;
// CanvasEngine.Square.prototype['setY'] = CanvasEngine.Square.prototype.setY;
/*jshint ignore:end */
