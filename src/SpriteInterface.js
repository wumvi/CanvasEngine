'use strict';
/* global CanvasEngine */



/**
 * @interface
 */
CanvasEngine.SpriteInterface = function() {
};


/**
 * Координаты отрисовки
 *
 * @return {CanvasEngine.Coordinates} Coordinates
 */
CanvasEngine.SpriteInterface.prototype.getCoordinates = function() {
};


/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {number} timestamp
 */
CanvasEngine.SpriteInterface.prototype.draw = function(ctx, timestamp) {
};


/**
 * Получаем объект для отрисоки
 * @param {string=} opt_name
 * @return {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement}
 */
CanvasEngine.SpriteInterface.prototype.getSurface = function(opt_name) {
};


/**
 * Рисовать ли объект
 *
 * @return {boolean} Draw surface or not
 */
CanvasEngine.SpriteInterface.prototype.isDraw = function() {
};


/**
 *
 * @param {number} index
 */
CanvasEngine.SpriteInterface.prototype.setZIndex = function(index) {
};


/**
 * Получаем приоритет слоя
 * @return {number}
 */
CanvasEngine.SpriteInterface.prototype.getZIndex = function() {
};
