'use strict';
/* global CanvasEngine */



/**
 *
 * @constructor
 */
CanvasEngine.Intersect = function() {
    /**
     *
     * @type {Array.<CanvasEngine.IntersectObject>}
     * @private
     */
    this.objectList_ = [];
};


/**
 *
 * @param {CanvasEngine.IntersectObject} obj
 */
CanvasEngine.Intersect.prototype.addObject = function(obj) {
    this.objectList_.push(obj);
};


/**
 *
 * @param {Array.<CanvasEngine.IntersectObject>} list
 */
CanvasEngine.Intersect.prototype.setObjectList = function(list) {
    this.objectList_ = list;
};


/**
 *
 * @param {CanvasEngine.Coordinates} p1
 * @param {CanvasEngine.Coordinates} p2
 * @param {number} x
 * @return {number}
 */
CanvasEngine.Intersect.lineFuncX = function(p1, p2, x) {
    return (x - p1.getX()) * (p2.getY() - p1.getY()) / (p2.getX() - p1.getX()) + p1.getY();
};


/**
 *
 * @param {CanvasEngine.Coordinates} p1
 * @param {CanvasEngine.Coordinates} p2
 * @param {number} y
 * @return {number}
 */
CanvasEngine.Intersect.lineFuncY = function(p1, p2, y) {
    return (y - p1.getY()) * (p2.getX() - p1.getX()) / (p2.getY() - p1.getY()) + p1.getX();
};


/**
 * Проверяем пересечение отрезка с объектом
 * @param {CanvasEngine.IntersectObject} obj Объект с которым проверяем пересечение
 * @param {CanvasEngine.Coordinates} p1 Точка 1
 * @param {CanvasEngine.Coordinates} p2 Точка 2
 * @return {CanvasEngine.IntersectObject} Объект пересечения с доп данными
 */
CanvasEngine.Intersect.prototype.getIntersection = function(obj, p1, p2) {
    var x, y;

    /**
     * @type {Array.<CanvasEngine.Coordinates>}
     */
    var crossList = [];

    var pointList = [p1.getX(), p2.getX()].sort(function(a, b) {
        return a > b ? 1 : (a < b ? -1 : 0);
    });

    var p1x = pointList[0];
    var p2x = pointList[1];

    pointList = [p1.getY(), p2.getY()].sort(function(a, b) {
        return a > b ? 1 : (a < b ? -1 : 0);
    });

    var p1y = pointList[0];
    var p2y = pointList[1];

    var isParallelY = p1y === p2y;
    if (isParallelY) {
        y = p1y;
        if (obj.getY1() <= y && y <= obj.getY2()) {
            // left line
            if (p1x <= obj.getX1() && obj.getX1() < p2x) {
                crossList.push(new CanvasEngine.Coordinates(obj.getX1(), y));
            }

            // right line
            if (p1x <= obj.getX2() && obj.getX2() < p2x) {
                crossList.push(new CanvasEngine.Coordinates(obj.getX2(), y));
            }
        }
    }

    var isParallelX = p1x === p2x;
    if (isParallelX) {
        x = p1x;
        if (obj.getX1() <= x && x <= obj.getX2()) {
            // bottom line
            if (p1y <= obj.getY1() && obj.getY1() < p2y) {
                crossList.push(new CanvasEngine.Coordinates(x, obj.getY1()));
            }

            // top line
            if (p1y <= obj.getY2() && obj.getY2() < p2y) {
                crossList.push(new CanvasEngine.Coordinates(x, obj.getY2()));
            }
        }
    }

    if (!isParallelX && !isParallelY) {
        // left line. cross point x, y1
        x = obj.getX1();
        y = CanvasEngine.Intersect.lineFuncX(p1, p2, x);
        if (obj.getY1() <= y && y <= obj.getY2() && p1y <= y && y <= p2y) {
            crossList.push(new CanvasEngine.Coordinates(x, Math.round(y)));
        }

        // right line. cross point x, y1
        x = obj.getX2();
        y = CanvasEngine.Intersect.lineFuncX(p1, p2, x);
        if (obj.getY1() <= y && y <= obj.getY2() && p1y <= y && y <= p2y) {
            crossList.push(new CanvasEngine.Coordinates(x, Math.round(y)));
        }


        // bottom line. cross point x, y1
        y = obj.getY1();
        x = CanvasEngine.Intersect.lineFuncY(p1, p2, y);
        if (obj.getX1() <= x && x <= obj.getX2() && p1x <= x && x <= p2x) {
            crossList.push(new CanvasEngine.Coordinates(Math.round(x), y));
        }

        // top line
        y = obj.getY2();
        x = CanvasEngine.Intersect.lineFuncY(p1, p2, y);
        if (obj.getX1() <= x && x <= obj.getX2() && p1x <= x && x <= p2x) {
            crossList.push(new CanvasEngine.Coordinates(Math.round(x), y));
        }
    }

    if (crossList.length === 0) {
        return null;
    }

    crossList = crossList.sort(this.sortCrossPoint_.bind(this, p1));
    var freePoint = this.getFreePoint(crossList[0].clone());
    obj.setIntersectResult(new CanvasEngine.IntersectResult(crossList[0], freePoint));

    return obj;
};


/**
 *
 * @param {CanvasEngine.Coordinates} p1
 * @param {CanvasEngine.Coordinates} item1
 * @param {CanvasEngine.Coordinates} item2
 * @private
 * @return {number}
 */
CanvasEngine.Intersect.prototype.sortCrossPoint_ = function(p1, item1, item2) {
    var len1 = Math.pow(item1.getX() - p1.getX(), 2) + Math.pow(item1.getY() - p1.getY(), 2);
    var len2 = Math.pow(item2.getX() - p1.getX(), 2) + Math.pow(item2.getY() - p1.getY(), 2);
    return len1 > len2 ? 1 : (len1 < len2 ? -1 : 0);
};


/**
 *
 * @param {CanvasEngine.Coordinates} crossPoint
 * @return {?CanvasEngine.Coordinates}
 */
CanvasEngine.Intersect.prototype.getFreePoint = function(crossPoint) {
    var x = crossPoint.getX();
    var y = crossPoint.getY();
    // TODO: происходит одно лишнее сравнение
    for (var xo = -1; xo <= 1; xo += 1) {
        for (var yo = -1; yo <= 1; yo += 1) {
            crossPoint.setXY(x - xo, y - yo);
            if (this.getFreePointCheck_(crossPoint)) {
                return crossPoint;
            }
        }
    }

    return null;
};


/**
 *
 * @param {CanvasEngine.Coordinates} crossPoint
 * @return {boolean}
 * @private
 */
CanvasEngine.Intersect.prototype.getFreePointCheck_ = function(crossPoint) {
    var length = this.objectList_.length;
    var flag = true;
    for (var num = 0; num < length; num += 1) {
        if (this.objectList_[num].isPointInObject(crossPoint)) {
            flag = false;
            break;
        }
    }

    return flag;
};


/**
 * Проверяем есть ли пересечения с объектами
 * @param {CanvasEngine.Coordinates} lastPosCursor Прошло положение курсора
 * @param {CanvasEngine.Coordinates} currentPosCursor Текущее положение курсора
 *
 * @return {Array.<CanvasEngine.IntersectObject>} Массив объектов, которые мы пересекли с доп. данными
 */
CanvasEngine.Intersect.prototype.getIntersectObjectList = function(lastPosCursor, currentPosCursor) {
    var list = [];
    var length = this.objectList_.length;
    for (var num = 0; num < length; num += 1) {
        if (!this.objectList_[num].isEnable()) {
            continue;
        }

        var obj = this.getIntersection(
            this.objectList_[num],
            lastPosCursor,
            currentPosCursor
        );

        if (obj === null) {
            continue;
        }

        list.push(obj);
    }

    return list.sort(this.sortCallback_.bind(this, lastPosCursor));
};


/**
 *
 * @const {number}
 */
CanvasEngine.Intersect.LEFT = 0;


/**
 *
 * @const {number}
 */
CanvasEngine.Intersect.BOTTOM = 0;


/**
 *
 * @const {number}
 */
CanvasEngine.Intersect.RIGHT = 1;


/**
 *
 * @const {number}
 */
CanvasEngine.Intersect.TOP = 1;


/**
 * @param {CanvasEngine.Coordinates} p1
 * @param {CanvasEngine.IntersectObject} obj1
 * @param {CanvasEngine.IntersectObject} obj2
 * @return {number}
 * @private
 */
CanvasEngine.Intersect.prototype.sortCallback_ = function(p1, obj1, obj2) {
    var item1 = obj1.getIntersectResult().getCrossPoint();
    var item2 = obj2.getIntersectResult().getCrossPoint();
    var len1 = Math.pow(item1.getX() - p1.getX(), 2) + Math.pow(item1.getY() - p1.getY(), 2);
    var len2 = Math.pow(item2.getX() - p1.getX(), 2) + Math.pow(item2.getY() - p1.getY(), 2);
    return len1 > len2 ? 1 : (len1 < len2 ? -1 : 0);
};


/**
 * Очищаем список объектов
 */
CanvasEngine.Intersect.prototype.clearObjectList = function() {
    this.objectList_ = [];
};
