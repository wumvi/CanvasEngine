'use strict';
/* global CanvasEngine, goog */



/**
 *
 * @constructor
 */
CanvasEngine.Loader = function() {
  this.resourcesCount_ = 0;
  this.callbacksList_ = {};

  /**
   *
   * @type {Object}
   * @private
   */
  this.returnData_ = {};
};


/**
 *
 * @param {Array.<CanvasEngine.ResLoadType>} resList
 */
CanvasEngine.Loader.prototype.load = function(resList) {
  var that = this;
  this.resourcesCount_ = 0;

  if (!Array.isArray(resList)) {
    throw new Error('The param \'resList\' must be Array');
  }

  if (resList.length === 0) {
    this.checkIsDone();
    return;
  }

  jQuery.each(resList, function(num, obj) {
    if (obj.getData() instanceof CanvasEngine.ResLoadTypeJson) {
      that.resourcesCount_ += 1;
    } else {
      that.resourcesCount_ += Object.keys(obj.getData()).length;
    }
  });

  for (var num = 0; num < resList.length; num += 1) {
    if (resList[num] instanceof CanvasEngine.ResLoadType) {
      this.loadBlock_(resList[num]);
    } else {
      throw new Error('resList must be Array.<ResLoadType>');
    }
  }
};


/**
 *
 * @param {string|number} eventName
 * @param {Function} callback
 */
CanvasEngine.Loader.prototype.addEventListener = function(eventName, callback) {
  this.callbacksList_[eventName] = callback;
};


/**
 *
 */
CanvasEngine.Loader.prototype.checkIsDone = function() {
  if (this.resourcesCount_ === 0) {
    this.callbacksList_[CanvasEngine.Loader.DONE](
      new CanvasEngine.ResLoadEvent(CanvasEngine.Loader.DONE, this.returnData_)
    );
  }
};


/**
 *
 * @param {string} blockName
 * @param {Object} jsonData
 * @private
 */
CanvasEngine.Loader.prototype.parseJson_ = function(blockName, jsonData) {
  // Object.keys(jsonData.list).length;
  /*for(var name in jsonData) {
   if (!jsonData.hasOwnProperty(name)) {
   continue;
   }

   this.returnData_[blockName][name] = jsonData[name];
   this.loadImage_(blockName, name, jsonData[name].url);
   }*/

  this.returnData_[blockName].sprite = jsonData;
  this.loadImage_(blockName, 'sprite', jsonData.url);
};


/**
 *
 * @param {CanvasEngine.ResLoadType} resLoad
 * @private
 */
CanvasEngine.Loader.prototype.loadBlock_ = function(resLoad) {
  var that = this;

  var resLoadData = resLoad.getData();
  var resLoadName = resLoad.getName();

  this.returnData_[resLoadName] = {};

  if (resLoadData instanceof CanvasEngine.ResLoadTypeJson) {
    jQuery.getJSON(resLoadData.getUrl(), function(data) {
      that.parseJson_(resLoadName, data);
    });
    return;
  }

  for (var imgName in resLoadData) {
    if (!resLoadData.hasOwnProperty(imgName)) {
      continue;
    }

    this.returnData_[resLoadName][imgName] = {};
    this.loadImage_(resLoadName, imgName, resLoadData[imgName]);
  }
};


/**
 *
 * @param {string} groupName
 * @param {string} imgName
 * @param {string} url
 * @private
 */
CanvasEngine.Loader.prototype.loadImage_ = function(groupName, imgName, url) {
  var that = this;

  var img = /** @type {HTMLImageElement} */ (document.createElement('img'));
  img.onload = function() {
    // console.log('that.resourcesCount_ = ', that.resourcesCount_, ' ', url);
    that.returnData_[groupName][imgName].img = this;
    that.resourcesCount_ -= 1;
    that.checkIsDone();
  };

  img.onerror = function() {
    that.resourcesCount_ -= 1;
    that.checkIsDone();
  };

  img.src = url;
};


/**
 * @const {string}
 */
CanvasEngine.Loader.DONE = 'load.done';

/*jshint ignore:start */
// window['CanvasEngine']['Loader'] = CanvasEngine.Loader;
// CanvasEngine.Loader.prototype['load'] = CanvasEngine.Loader.prototype.load;
// CanvasEngine.Loader.prototype['addEventListener'] = CanvasEngine.Loader.prototype.addEventListener;
// CanvasEngine.Loader['DONE'] = CanvasEngine.Loader.DONE;
/*jshint ignore:end */
