'use strict';
/* global CanvasEngine, goog */



/**
 * @param {?CanvasEngine.GameCanvas=} opt_gameCanvas = null opt_gameCanvas
 * @constructor
 * @implements {CanvasEngine.SpriteInterface}
  */
CanvasEngine.Sprite = function(opt_gameCanvas) {
  /**
   * List of surfaces object
   * @type {Object}
   * @private
   */
  this.surfacesList_ = {};

  /**
   * Current number of surface in surface's list
   * @type {string}
   * @private
   */
  this.currentSurface_ = 'def';

  /**
   * Flag for draw surface or not
   * @type {boolean}
   * @private
   */
  this.isDraw_ = true;

  /**
   * Coordinates
   * @type {CanvasEngine.Coordinates}
   * @private
   */
  this.coordinates_ = new CanvasEngine.Coordinates(0, 0);

  /**
   * Width
   * @type {number}
   * @private
   */
  this.widthCanvas_ = -1;

  /**
   * Height
   * @type {number}
   * @private
   */
  this.heightCanvas_ = -1;

  /**
   * @type {Object}
   * @private
   */
  this.imgInfo_ = {};

  /**
   *
   * @type {Object}
   * @private
   */
  this.animInfo_ = {};

  /**
   * @private
   * @type {number}
   */
  this.animTimer_ = 0;

  /**
   *
   * @type {Array.<string>}
   * @private
   */
  this.animInfoKeyList_ = [];

  /**
   * @private
   * @type {number}
   */
  this.animInfoKeyCurrent_ = 0;

  /**
   *
   * @type {?CanvasEngine.GameCanvas}
   * @private
   */
  this.gameCanvas_ = opt_gameCanvas ? opt_gameCanvas : null;

  /**
   *
   * @type {number}
   * @private
   */
  this.zIndex_ = 0;
};


/**
 * Получаем приоритет слоя
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getZIndex = function() {
  return this.zIndex_;
};


/**
 * Устанавливаем приоритет слоя
 * @param {number} zIndex
 */
CanvasEngine.Sprite.prototype.setZIndex = function(zIndex) {
  this.zIndex_ = zIndex;
};


/**
 *
 * @param {number} speed
 * @return {CanvasEngine.SpeedCoordination}
 */
CanvasEngine.Sprite.prototype.makeSpeedCoordination = function(speed) {
  return new CanvasEngine.SpeedCoordination(speed, this.gameCanvas_);
};


/**
 *
 * @param {CanvasRenderingContext2D} ctx
 * @param {string} animKey
 */
CanvasEngine.Sprite.prototype.drawSlide = function(ctx, animKey) {
  this.drawSlideWithXY(
    ctx,
    animKey,
    this.getCoordinates().getX(),
    this.getCoordinates().getY()
  );
};


/**
 *
 * @param {CanvasRenderingContext2D} ctx
 * @param {string} animKey
 * @param {number} x
 * @param {number} y
 */
CanvasEngine.Sprite.prototype.drawSlideWithXY = function(ctx, animKey, x, y) {
  ctx.drawImage(
    this.getSurface(),
    this.imgInfo_[animKey]['x'],
    this.imgInfo_[animKey]['y'],
    this.imgInfo_[animKey]['width'],
    this.imgInfo_[animKey]['height'],
    x,
    y,
    this.imgInfo_[animKey]['width'],
    this.imgInfo_[animKey]['height']
  );
};


/**
 *
 * @param {CanvasRenderingContext2D} ctx
 * @param {HTMLImageElement} img
 * @param {Object} obj
 * @param {number} x
 * @param {number} y
 */
CanvasEngine.Sprite.prototype.drawPartImgWithXY = function(ctx, img, obj, x, y) {
  ctx.drawImage(
    img,
    obj.x,
    obj.y,
    obj.width,
    obj.height,
    x,
    y,
    obj.width,
    obj.height
  );
};


/**
 *
 * @param {CanvasEngine.GameCanvas} gameCanvas
 */
CanvasEngine.Sprite.prototype.setGameCanvas = function(gameCanvas) {
  this.gameCanvas_ = gameCanvas;
};


/**
 *
 * @return {CanvasEngine.GameCanvas}
 */
CanvasEngine.Sprite.prototype.getGameCanvas = function() {
  return this.gameCanvas_;
};


/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {number} timestamp
 */
CanvasEngine.Sprite.prototype.drawAnimation = function(ctx, timestamp) {
  var animKey = this.animInfoKeyList_[this.animInfoKeyCurrent_];

  this.drawSlide(ctx, animKey);

  if (timestamp - this.animTimer_ > this.animInfo_[animKey].time) {
    this.nextAnimationKey();
    this.animTimer_ = timestamp;
  }
};


/**
 */
CanvasEngine.Sprite.prototype.nextAnimationKey = function() {
  this.animInfoKeyCurrent_ += 1;
  if (this.animInfoKeyCurrent_ === this.animInfoKeyList_.length) {
    this.animInfoKeyCurrent_ = 0;
  }
};


/**
 *
 * @param {!Object} imgInfo
 */
CanvasEngine.Sprite.prototype.setSpriteInfo = function(imgInfo) {
  this.imgInfo_ = imgInfo;
};


/**
 *
 * @param {!Object} imgInfo
 * @param {!Object} animationInfo
 */
CanvasEngine.Sprite.prototype.setAnimationInfo = function(imgInfo, animationInfo) {
  this.imgInfo_ = imgInfo;
  this.animInfo_ = animationInfo;
  this.animInfoKeyList_ = Object.keys(this.animInfo_);
};


/**
 *
 * @param {CanvasEngine.Coordinates} coordinates
 */
CanvasEngine.Sprite.prototype.linkCoordinates = function(coordinates) {
  this.setCoordinates(coordinates);
};


/**
 *
 * @return {CanvasEngine.Coordinates}
 */
CanvasEngine.Sprite.prototype.getCoordinates = function() {
  return this.coordinates_;
};


/**
 *
 * @param {CanvasEngine.Coordinates} coordinates
 */
CanvasEngine.Sprite.prototype.setCoordinates = function(coordinates) {
  this.coordinates_ = coordinates;
};


/**
 *
 * @param {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement} surface
 * @param {string=} opt_key
 *
 * @return {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement}
 */
CanvasEngine.Sprite.prototype.setSurface = function(surface, opt_key) {
  opt_key = opt_key ? opt_key : this.currentSurface_;

  if (!(surface instanceof HTMLImageElement ||
    surface instanceof HTMLCanvasElement ||
    surface instanceof HTMLVideoElement)) {
    throw new Error('Failed to execute \'setSurface\' on \'Sprite\': The provided value is not of type ' +
      '\'(HTMLImageElement or HTMLVideoElement or HTMLCanvasElement or ImageBitmap)\'');
  }

  this.surfacesList_[opt_key] = surface;
  return surface;
};


/**
 *
 * @param {string} key
 */
CanvasEngine.Sprite.prototype.setCurrentSurfaceKey = function(key) {
  this.currentSurface_ = key;
};


/**
 *
 * @param {string=} opt_name
 * @return {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement}
 */
CanvasEngine.Sprite.prototype.getSurface = function(opt_name) {
  return this.surfacesList_[opt_name ? opt_name : this.currentSurface_];
};


/**
 *
 * @return {boolean}
 */
CanvasEngine.Sprite.prototype.isDraw = function() {
  return this.isDraw_;
};


/**
 *
 */
CanvasEngine.Sprite.prototype.hide = function() {
  this.isDraw_ = false;
};


/**
 *
 */
CanvasEngine.Sprite.prototype.show = function() {
  this.isDraw_ = true;
};


/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {number} timestamp
 */
CanvasEngine.Sprite.prototype.draw = function(ctx, timestamp) {
  throw new Error('You must overload this method: CanvasEngine.Sprite.prototype.draw');
};


/**
 *
 * @param {HTMLImageElement|HTMLCanvasElement} surface
 * @param {string=} opt_name = '' opt_name Название поверхности
 */
CanvasEngine.Sprite.prototype.addSurface = function(surface, opt_name) {
  var name = opt_name ? opt_name : this.currentSurface_;
  this.surfacesList_[name] = surface;
};


/**
 * Уставливаем ширину спрайта
 * @param {number} width Ширина
 */
CanvasEngine.Sprite.prototype.setWidth = function(width) {
  this.widthCanvas_ = width;
};


/**
 * Уставливаем высоту спрайта
 * @param {number} height Высота
 */
CanvasEngine.Sprite.prototype.setHeight = function(height) {
  this.heightCanvas_ = height;
};


/**
 * Ширина спрайта
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getWidth = function() {
  return this.widthCanvas_;
};


/**
 * Высота спрайта
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getHeight = function() {
  return this.heightCanvas_;
};


/**
 * Получаем горизонтальный центр спрайта
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getHorizontalCenter = function() {
  return this.coordinates_.getX() + this.widthCanvas_ / 2 >> 0;
};


/**
 * Получаем вертикальный центр спрайта
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getVerticalCenter = function() {
  return this.coordinates_.getY() + this.heightCanvas_ / 2 >> 0;
};


/**
 *
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getVertTop = function() {
  return this.coordinates_.getY();
};


/**
 *
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getVertBottom = function() {
  return this.coordinates_.getY() + this.heightCanvas_;
};


/**
 *
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getHorizLeft = function() {
  return this.coordinates_.getX();
};


/**
 *
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getHorizRight = function() {
  return this.coordinates_.getX() + this.widthCanvas_;
};


/**
 *
 * @param {number} speed
 */
CanvasEngine.Sprite.prototype.moveHorizontal = function(speed) {
  if (speed === 0) {
    return;
  }

  this.coordinates_.incX(speed);
};


/**
 *
 * @param {number} speed
 */
CanvasEngine.Sprite.prototype.moveVertical = function(speed) {
  if (speed === 0) {
    return;
  }

  this.coordinates_.incY(speed);
};

