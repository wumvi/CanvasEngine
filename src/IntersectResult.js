'use strict';
/* global CanvasEngine */



/**
 * @param {CanvasEngine.Coordinates} crossPoint
 * @param {CanvasEngine.Coordinates} freePoint
 * @constructor
 */
CanvasEngine.IntersectResult = function(crossPoint, freePoint) {
    /**
     *
     * @type {CanvasEngine.Coordinates}
     * @private
     */
    this.freePoint_ = freePoint;

    /**
     *
     * @type {CanvasEngine.Coordinates}
     * @private
     */
    this.crossPoint_ = crossPoint;
};


/**
 *
 * @return {CanvasEngine.Coordinates}
 */
CanvasEngine.IntersectResult.prototype.getFreePoint = function() {
    return this.freePoint_;
};


/**
 *
 * @return {CanvasEngine.Coordinates}
 */
CanvasEngine.IntersectResult.prototype.getCrossPoint = function() {
    return this.crossPoint_;
};
