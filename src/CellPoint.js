'use strict';
/* global CanvasEngine, goog */



/**
 *
 * @param {number} x
 * @param {number} y
 * @constructor
 */
CanvasEngine.CellPoint = function(x, y) {
  /**
   *
   * @type {number}
   */
  this.x = x;

  /**
   *
   * @type {number}
   */
  this.y = y;
};


/**
 *
 * @return {number}
 */
CanvasEngine.CellPoint.prototype.getX = function() {
  return this.x;
};


/**
 *
 * @return {number}
 */
CanvasEngine.CellPoint.prototype.getY = function() {
  return this.y;
};


/**
 *
 * @param {number} x
 */
CanvasEngine.CellPoint.prototype.setX = function(x) {
  this.x = x;
};


/**
 *
 * @param {number} y
 */
CanvasEngine.CellPoint.prototype.setY = function(y) {
  this.y = y;
};



/*jshint ignore:start */
// window['CanvasEngine']['CellPoint'] = CanvasEngine.CellPoint;
// CanvasEngine.CellPoint.prototype['getX'] = CanvasEngine.CellPoint.prototype.getX;
// CanvasEngine.CellPoint.prototype['getY'] = CanvasEngine.CellPoint.prototype.getY;
// CanvasEngine.CellPoint.prototype['setX'] = CanvasEngine.CellPoint.prototype.setX;
// CanvasEngine.CellPoint.prototype['setY'] = CanvasEngine.CellPoint.prototype.setY;
/*jshint ignore:end */
