'use strict';
/* global CanvasEngine */



/**
 * Поддержка pointerLock
 * @param {HTMLElement} element HTML элемент
 * @constructor
 */
CanvasEngine.CursorLock = function(element) {
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this.element_ = element;

    /**
     * Заблокировал ли объект
     * @type {boolean}
     * @private
     */
    this.isLock_ = false;

    /**
     * Старые координаты мыши. Ось X
     * @type {number}
     * @private
     */
    this.lastMouseX_ = 0;

    /**
     * Старые координаты мыши. Ось Y
     * @type {number}
     * @private
     */
    this.lastMouseY_ = 0;

    this.init_();
};


/**
 * Инициализация
 * @private
 */
CanvasEngine.CursorLock.prototype.init_ = function() {
    this.element_.requestPointerLock = this.element_.requestPointerLock ||
        this.element_.mozRequestPointerLock || this.element_.webkitRequestPointerLock;

    document.exitPointerLock = document.exitPointerLock ||
        document.mozExitPointerLock || document.webkitExitPointerLock;

    this.initEvent_();
};


/**
 *
 * @private
 */
CanvasEngine.CursorLock.prototype.initEvent_ = function() {
    document.addEventListener('pointerlockchange', this.onLockStateChange_.bind(this), false);
    document.addEventListener('mozpointerlockchange', this.onLockStateChange_.bind(this), false);
    document.addEventListener('webkitpointerlockchange', this.onLockStateChange_.bind(this), false);
};


/**
 * Смена статуса блакировки
 * @private
 */
CanvasEngine.CursorLock.prototype.onLockStateChange_ = function() {
    this.isLock_ = !this.isLock_;
};


/**
 * Заблокирова ли объект
 * @return {boolean}
 */
CanvasEngine.CursorLock.prototype.isLock = function() {
    return this.isLock_;
};


/**
 * Блокировка курсора
 * @param {number} lastMouseX
 * @param {number} lastMouseY
 */
CanvasEngine.CursorLock.prototype.lock = function(lastMouseX, lastMouseY) {
    this.lastMouseX_ = lastMouseX;
    this.lastMouseY_ = lastMouseY;
    this.element_.requestPointerLock();
};


/**
 * Разблокировка курсора
 */
CanvasEngine.CursorLock.prototype.unlock = function() {
    document.exitPointerLock();
};


/**
 * @param {Event} event
 * @return {CanvasEngine.Coordinates}
 */
CanvasEngine.CursorLock.prototype.getMouseCoordinates = function(event) {
    var x = event['movementX'] || event['mozMovementX'] || event['webkitMovementX'] || 0;
    var y = event['movementY'] || event['mozMovementY'] || event['webkitMovementY'] || 0;
    this.lastMouseX_ += x;
    this.lastMouseY_ += y;
    return new CanvasEngine.Coordinates(this.lastMouseX_, this.lastMouseY_);
};


/**
 * Поддерживается ли pointerLock
 * @const {boolean}
 */
CanvasEngine.CursorLock.IS_POINT_LOCK_SUPPORT = 'pointerLockElement' in document ||
    'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;
