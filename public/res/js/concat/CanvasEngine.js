'use strict';
/**
 *
 * @constructor
 * @export
 */
function CanvasEngine() {
}
/* global CanvasEngine, goog */
/**
 * Класс для созданий различных анимаций
 *
 * @param {number} begin Начальная велична
 * @param {number} end Конечная величина
 * @param {number} speed Конечная величина
 * @param {CanvasEngine.GameCanvas} gameCanvas
 * @constructor
 */
CanvasEngine.AnimationValue = function (begin, end, speed, gameCanvas) {
    this.speed_ = new CanvasEngine.SpeedCoordination(speed, gameCanvas);
    this.isPause_ = true;
    this.setInfo(begin, end);    /**
   *
   * @type {?CanvasEngine.GameCanvas}
   * @private
   */
                                 // this.gameCanvas_ = gameCanvas;
};
/**
 *
 * @param {number} begin Начальная велична
 * @param {number} end Конечная величина
 */
CanvasEngine.AnimationValue.prototype.setInfo = function (begin, end) {
    this.begin_ = begin;
    this.end_ = end;
    this.value_ = this.begin_;
    this.direction_ = begin < end ? 1 : -1;
};
/**
 * Запуск анимации
 */
CanvasEngine.AnimationValue.prototype.start = function () {
    this.isPause_ = false;
    this.reset();
};
/**
 *
 */
CanvasEngine.AnimationValue.prototype.reset = function () {
    this.value_ = this.begin_;
};
/**
 * Стоит режим паузы?
 * @return {boolean} Режим паузы?
 */
CanvasEngine.AnimationValue.prototype.isPause = function () {
    return this.isPause_;
};
/**
 *
 * @param {number} direction
 */
CanvasEngine.AnimationValue.prototype.setDirection = function (direction) {
    this.direction_ = direction;
};
/**
 * Получаем значение
 * @return {number} Значение
 */
CanvasEngine.AnimationValue.prototype.getValue = function () {
    if (this.isPause_) {
        return this.value_;
    }
    this.value_ += this.direction_ * this.speed_.getSpeed();
    if (this.direction_ === -1 && this.value_ < this.end_) {
        this.value_ = this.end_;
        this.isPause_ = true;
        return this.value_;
    } else if (this.direction_ === 1 && this.value_ > this.end_) {
        this.value_ = this.end_;
        this.isPause_ = true;
        return this.value_;
    }
    return this.value_;
};
/* global CanvasEngine, goog */
/**
 *
 * @param {number} x
 * @param {number} y
 * @constructor
 */
CanvasEngine.CellPoint = function (x, y) {
    /**
   *
   * @type {number}
   */
    this.x = x;
    /**
   *
   * @type {number}
   */
    this.y = y;
};
/**
 *
 * @return {number}
 */
CanvasEngine.CellPoint.prototype.getX = function () {
    return this.x;
};
/**
 *
 * @return {number}
 */
CanvasEngine.CellPoint.prototype.getY = function () {
    return this.y;
};
/**
 *
 * @param {number} x
 */
CanvasEngine.CellPoint.prototype.setX = function (x) {
    this.x = x;
};
/**
 *
 * @param {number} y
 */
CanvasEngine.CellPoint.prototype.setY = function (y) {
    this.y = y;
};
/* global CanvasEngine, goog */
/**
 * Модель для события мыши
 * @param {Object} event
 * @param {HTMLElement=} opt_canvasDomElement
 * @constructor
 */
CanvasEngine.ClickEventBridge = function (event, opt_canvasDomElement) {
    /*jshint ignore:start */
    var mainCanvasOffset = jQuery(opt_canvasDomElement).offset();
    if (mainCanvasOffset) {
        this.mouseX_ = event['originalEvent']['touches'][0]['pageX'] - mainCanvasOffset['left'];
        this.mouseY_ = event['originalEvent']['touches'][0]['pageY'] - mainCanvasOffset['top'];
    } else {
        this.mouseX_ = event['offsetX'];
        this.mouseY_ = event['offsetY'];
    }    /*jshint ignore:end */
};
/**
 * Получаем позицию X
 * @return {number}
 */
CanvasEngine.ClickEventBridge.prototype.getX = function () {
    return this.mouseX_;
};
/**
 * Получаем позицию Y
 * @return {number}
 */
CanvasEngine.ClickEventBridge.prototype.getY = function () {
    return this.mouseY_;
};
/* global CanvasEngine */
/**
 * Модель координат
 * @param {number} x Координата X
 * @param {number} y Координата Y
 * @constructor
 */
CanvasEngine.Coordinates = function (x, y) {
    /**
   * Координата X
   * @type {number}
   * @private
   */
    this.x_ = x;
    /**
   * Координата Y
   * @type {number}
   * @private
   */
    this.y_ = y;
};
/**
 * Получаем координату X
 * @return {number}
 */
CanvasEngine.Coordinates.prototype.getX = function () {
    return this.x_;
};
/**
 * Получаем координату Y
 * @return {number}
 */
CanvasEngine.Coordinates.prototype.getY = function () {
    return this.y_;
};
/**
 * Устанавливаем координату X
 * @param {number} x
 */
CanvasEngine.Coordinates.prototype.setX = function (x) {
    this.x_ = x;
};
/**
 * Устанавливаем координату Y
 * @param {number} y
 */
CanvasEngine.Coordinates.prototype.setY = function (y) {
    this.y_ = y;
};
/**
 * Увеличиваем координату X на величину val
 * @param {number} val
 */
CanvasEngine.Coordinates.prototype.incX = function (val) {
    this.x_ += val;
};
/**
 * Увеличиваем координату Y на величину val
 * @param {number} val
 */
CanvasEngine.Coordinates.prototype.incY = function (val) {
    this.y_ += val;
};
/**
 * Устанавливаем сразу координату X и Y
 * @param {number} x
 * @param {number} y
 */
CanvasEngine.Coordinates.prototype.setXY = function (x, y) {
    this.x_ = x;
    this.y_ = y;
};
/**
 *
 * @param {CanvasEngine.Coordinates} coordinates
 */
CanvasEngine.Coordinates.prototype.set = function (coordinates) {
    this.x_ = coordinates.getX();
    this.y_ = coordinates.getY();
};
/* global CanvasEngine */
/**
 *
 * @param {number} width Width of canvas
 * @param {number} height Height of canvas
 * @constructor
 */
CanvasEngine.GameCanvas = function (width, height) {
    /**
   * List of sprite's object
   * @type {Array.<CanvasEngine.SpriteInterface>}
   * @private
   */
    this.surfacesList_ = [];
    /**
   * Dom canvas
   * @type {HTMLCanvasElement}
   * @private
   */
    this.htmlCanvas_ = document.createElement('canvas');
    /**
   * Context for Canvas
   * @type {CanvasRenderingContext2D}
   * @private
   */
    this.ctx_ = this.htmlCanvas_.getContext('2d');
    /**
   * Width of canvas
   * @type {number}
   * @private
   */
    this.widthCanvas_ = width;
    /**
   * Height of canvas
   * @type {number}
   * @private
   */
    this.heightCanvas_ = height;
    /**
   *
   * @type {boolean}
   * @private
   */
    this.isTick_ = false;
    /**
   *
   * @type {number}
   * @private
   */
    this.tickTimestamp_ = 0;
    /**
   *
   * @type {number}
   * @private
   */
    this.fpsTimestamp_ = 0;
    /**
   *
   * @type {number}
   * @private
   */
    this.fpsValue_ = 0;
    /**
   *
   * @type {Array.<Function>}
   * @private
   */
    this.tickFuncCallback_ = [];
    /**
   *
   * @type {Array.<Function>}
   * @private
   */
    this.reUpdateFpsCallbakList_ = [];
    this.init_();
};
/**
 * Init variables
 * @private
 */
CanvasEngine.GameCanvas.prototype.init_ = function () {
    this.htmlCanvas_.width = this.widthCanvas_;
    this.htmlCanvas_.height = this.heightCanvas_;
};
/**
 * Сортируем спрайты по ZIndex полю, выставляя приоритеты отрисовки
 */
CanvasEngine.GameCanvas.prototype.sortByZIndex = function () {
    var that = this;
    this.surfacesList_.sort(function (item1, item2) {
        return that.sortByZIndexCallback(item1, item2);
    });
};
/**
 *
 * @param {CanvasEngine.SpriteInterface} item1
 * @param {CanvasEngine.SpriteInterface} item2
 * @return {number}
 */
CanvasEngine.GameCanvas.prototype.sortByZIndexCallback = function (item1, item2) {
    return item1.getZIndex() - item2.getZIndex();
};
/**
 * Add sprite to list
 * @param {CanvasEngine.SpriteInterface} sprite
 */
CanvasEngine.GameCanvas.prototype.addSurface = function (sprite) {
    if (sprite instanceof CanvasEngine.Sprite) {
        this.surfacesList_.push(sprite);
    } else {
        throw new Error('sprite must be a SpriteInterface');
    }
};
/**
 *
 * @return {Array.<CanvasEngine.SpriteInterface>}
 */
CanvasEngine.GameCanvas.prototype.getSurfaceList = function () {
    return this.surfacesList_;
};
/**
 *
 * @param {number} offsetX
 * @param {number} offsetY
 */
CanvasEngine.GameCanvas.prototype.onCameraMove = function (offsetX, offsetY) {
    this.ctx_.translate(offsetX, offsetY);
};
/**
 *
 * @return {number}
 */
CanvasEngine.GameCanvas.prototype.getWidth = function () {
    return this.widthCanvas_;
};
/**
 *
 * @return {number}
 */
CanvasEngine.GameCanvas.prototype.getHeight = function () {
    return this.heightCanvas_;
};
/**
 *
 * @return {CanvasRenderingContext2D}
 */
CanvasEngine.GameCanvas.prototype.getContext = function () {
    return this.ctx_;
};
/**
 *
 * @type {number}
 */
CanvasEngine.GameCanvas.TICKS_PER_SECOND = 30;
/**
 *
 * @type {number}
 */
CanvasEngine.GameCanvas.skip_ticks = 1000 / CanvasEngine.GameCanvas.TICKS_PER_SECOND >> 0;
/**
 *
 * @return {boolean}
 */
CanvasEngine.GameCanvas.prototype.isTick = function () {
    return this.isTick_;
};
/**
 *
 * @param {Function} func
 */
CanvasEngine.GameCanvas.prototype.addTickCallback = function (func) {
    this.tickFuncCallback_.push(func);
};
/**
 * Repaint all surface
 * @param {number} timestamp
 */
CanvasEngine.GameCanvas.prototype.repaint = function (timestamp) {
    this.fpsValue_ += 1;
    if (this.fpsTimestamp_ <= timestamp) {
        this.fpsTimestamp_ = timestamp + 1000;
        CanvasEngine.GameCanvas.TICKS_PER_SECOND = this.fpsValue_;
        // >= 30 ? this.fpsValue_ : 30;
        this.fpsValue_ = 0;
        CanvasEngine.GameCanvas.skip_ticks = 1000 / CanvasEngine.GameCanvas.TICKS_PER_SECOND >> 0;
    }
    this.isTick_ = false;
    if (this.tickTimestamp_ < timestamp) {
        this.tickTimestamp_ += CanvasEngine.GameCanvas.skip_ticks;
        this.isTick_ = true;
        for (var i = 0; i < this.tickFuncCallback_.length; i += 1) {
            this.tickFuncCallback_[i](timestamp);
        }
    }
    this.ctx_.clearRect(0, 0, this.widthCanvas_, this.heightCanvas_);
    for (var num = 0; num < this.surfacesList_.length; num += 1) {
        if (!this.surfacesList_[num].isDraw()) {
            continue;
        }
        this.surfacesList_[num].draw(this.ctx_, timestamp);
    }
};
/**
 *
 * @return {HTMLCanvasElement}
 */
CanvasEngine.GameCanvas.prototype.getCanvas = function () {
    return this.htmlCanvas_;
};
/**
 *
 */
CanvasEngine.GameCanvas.prototype.reUpdateFps = function () {
    for (var num = 0; num < this.reUpdateFpsCallbakList_.length; num += 1) {
        this.reUpdateFpsCallbakList_[num]();
    }
};
/**
 *
 * @param {Function} cb
 */
CanvasEngine.GameCanvas.prototype.addReUpdateFpsNotifyCallback = function (cb) {
    this.reUpdateFpsCallbakList_.push(cb);
};
/* global CanvasEngine */
/**
 *
 * @constructor
 */
CanvasEngine.Intersect = function () {
    /**
     *
     * @type {Array.<CanvasEngine.IntersectObject>}
     * @private
     */
    this.objectList_ = [];
};
/**
 *
 * @param {CanvasEngine.IntersectObject} obj
 */
CanvasEngine.Intersect.prototype.addObject = function (obj) {
    this.objectList_.push(obj);
};
/**
 *
 * @param {Array.<CanvasEngine.IntersectObject>} list
 */
CanvasEngine.Intersect.prototype.setObjectList = function (list) {
    this.objectList_ = list;
};
/**
 *
 * @param {CanvasEngine.Coordinates} lastPosCursor
 * @param {CanvasEngine.Coordinates} currentPosCursor
 *
 * @return {Array.<CanvasEngine.IntersectResult>}
 */
CanvasEngine.Intersect.prototype.getIntersectObjectList = function (lastPosCursor, currentPosCursor) {
    var sideQuadX = lastPosCursor.getX() < currentPosCursor.getX() ? CanvasEngine.Intersect.LEFT : CanvasEngine.Intersect.RIGHT;
    var sideQuadY = lastPosCursor.getY() < currentPosCursor.getY() ? CanvasEngine.Intersect.BOTTOM : CanvasEngine.Intersect.TOP;
    var lineFuncX = function (line, x) {
        return (x - line.p1.getX()) * (line.p2.getY() - line.p1.getY()) / (line.p2.getX() - line.p1.getX()) + line.p1.getY();
    };
    var lineFuncY = function (line, y) {
        return (y - line.p1.getY()) * (line.p2.getX() - line.p1.getX()) / (line.p2.getY() - line.p1.getY()) + line.p1.getX();
    };
    var list = [];
    var length = this.objectList_.length;
    for (var num = 0; num < length; num += 1) {
        if (!this.objectList_[num].isEnable()) {
            continue;
        }
        var result = this.getIntersection(this.objectList_[num], lastPosCursor, currentPosCursor, sideQuadX, sideQuadY, lineFuncX, lineFuncY);
        if (result === null) {
            continue;
        }
        list.push(result);
    }
    return this.sort(list);
};
/**
 *
 * @const {number}
 */
CanvasEngine.Intersect.LEFT = 0;
/**
 *
 * @const {number}
 */
CanvasEngine.Intersect.BOTTOM = 0;
/**
 *
 * @const {number}
 */
CanvasEngine.Intersect.RIGHT = 1;
/**
 *
 * @const {number}
 */
CanvasEngine.Intersect.TOP = 1;
/**
 *
 * @param {Array.<CanvasEngine.IntersectResult>} list
 * @return {Array.<CanvasEngine.IntersectResult>}
 */
CanvasEngine.Intersect.prototype.sort = function (list) {
    return list.sort(this.sortCallback_.bind(this));
};
/**
 *
 * @param {CanvasEngine.IntersectResult} item1
 * @param {CanvasEngine.IntersectResult} item2
 * @private
 */
CanvasEngine.Intersect.prototype.sortCallback_ = function (item1, item2) {
    return item1.getDistance() > item2.getDistance();
};
/**
 * Очищаем список объектов
 */
CanvasEngine.Intersect.prototype.clearObjectList = function () {
    this.objectList_ = [];
};
/**
 *
 * @param {CanvasEngine.IntersectObject} obj
 * @param {CanvasEngine.Coordinates} lastPosCursor
 * @param {CanvasEngine.Coordinates} currentPosCursor
 * @param {number} sideQuadX
 * @param {number} sideQuadY
 * @param {function(Object, number)} lineFuncX
 * @param {function(Object, number)} lineFuncY
 * @return {CanvasEngine.IntersectResult|null}
 */
CanvasEngine.Intersect.prototype.getIntersection = function (obj, lastPosCursor, currentPosCursor, sideQuadX, sideQuadY, lineFuncX, lineFuncY) {
    var wall = {
            x: [obj.getX1(), obj.getX2()],
            y: [obj.getY1(), obj.getY2()]
        };
    var line = {
            p1: lastPosCursor,
            p2: currentPosCursor
        };
    var varSide, isIntersect, intersectY;
    var intersectX = wall.x[sideQuadX];
    if (line.p1.getY() === line.p2.getY()) {
        intersectY = line.p2.getY();
        varSide = sideQuadX === CanvasEngine.Intersect.LEFT ? [line.p1.getX(), line.p2.getX()] : [line.p2.getX(), line.p1.getX()];
        isIntersect = wall.y[0] <= intersectY && intersectY <= wall.y[1] && varSide[0] <= intersectX && intersectX <= varSide[1];
        return isIntersect ? new CanvasEngine.IntersectResult(obj, intersectX, intersectY, lastPosCursor) : null;
    }
    if (line.p1.getX() === line.p2.getX()) {
        intersectX = line.p2.getX();
        intersectY = wall.y[sideQuadY];
        varSide = sideQuadY === CanvasEngine.Intersect.BOTTOM ? [line.p1.getY(), line.p2.getY()] : [line.p2.getY(), line.p1.getY()];
        isIntersect = wall.x[0] <= intersectX && intersectX <= wall.x[1] && varSide[0] <= intersectY && intersectY <= varSide[1];
        return isIntersect ? new CanvasEngine.IntersectResult(obj, intersectX, intersectY, lastPosCursor) : null;
    }
    intersectY = lineFuncX(line, intersectX);
    isIntersect = wall.y[0] <= intersectY && intersectY <= wall.y[1];
    if (isIntersect) {
        if (sideQuadX === CanvasEngine.Intersect.LEFT && lastPosCursor.getX() < intersectX && intersectX <= currentPosCursor.getX()) {
            return new CanvasEngine.IntersectResult(obj, intersectX, intersectY, lastPosCursor);
        }
        if (sideQuadX === CanvasEngine.Intersect.RIGHT && currentPosCursor.getX() <= intersectX && intersectX < lastPosCursor.getX()) {
            return new CanvasEngine.IntersectResult(obj, intersectX, intersectY, lastPosCursor);
        }
    }
    intersectY = wall.y[sideQuadY];
    intersectX = lineFuncY(line, intersectY);
    isIntersect = wall.x[0] <= intersectX && intersectX <= wall.x[1];
    if (isIntersect) {
        if (sideQuadY === CanvasEngine.Intersect.BOTTOM && lastPosCursor.getY() < intersectY && intersectY <= currentPosCursor.getY()) {
            return new CanvasEngine.IntersectResult(obj, intersectX, intersectY, lastPosCursor);
        }
        if (sideQuadY === CanvasEngine.Intersect.TOP && currentPosCursor.getY() <= intersectY && intersectY < lastPosCursor.getY()) {
            return new CanvasEngine.IntersectResult(obj, intersectX, intersectY, lastPosCursor);
        }
    }
    return null;
};
/* global CanvasEngine */
/**
 * @param {number} x1
 * @param {number} x2
 * @param {number} y1
 * @param {number} y2
 * @constructor
 */
CanvasEngine.IntersectObject = function (x1, x2, y1, y2) {
    /**
     * @type {number}
     */
    this.x1_ = x1;
    /**
     * @type {number}
     */
    this.x2_ = x2;
    /**
     * @type {number}
     */
    this.y1_ = y1;
    /**
     * @type {number}
     */
    this.y2_ = y2;
    /**
     *
     * @type {boolean}
     * @private
     */
    this.enable_ = true;
};
/**
 *
 * @return {number}
 */
CanvasEngine.IntersectObject.prototype.getX1 = function () {
    return this.x1_;
};
/**
 *
 * @return {number}
 */
CanvasEngine.IntersectObject.prototype.getX2 = function () {
    return this.x2_;
};
/**
 *
 * @return {number}
 */
CanvasEngine.IntersectObject.prototype.getY1 = function () {
    return this.y1_;
};
/**
 *
 * @return {number}
 */
CanvasEngine.IntersectObject.prototype.getY2 = function () {
    return this.y2_;
};
/**
 *
 * @param {boolean} enable
 */
CanvasEngine.IntersectObject.prototype.setEnable = function (enable) {
    this.enable_ = enable;
};
/**
 *
 * @return {boolean}
 */
CanvasEngine.IntersectObject.prototype.isEnable = function () {
    return this.enable_;
};
/**
 *
 * @param {CanvasEngine.Coordinates} point
 * @return {boolean}
 */
CanvasEngine.IntersectObject.prototype.isPointInObject = function (point) {
    return this.x1_ <= point.getX() && point.getX() <= this.x2_ && this.y1_ <= point.getY() && point.getY() <= this.y2_;
};
/* global CanvasEngine */
/**
 * @param {CanvasEngine.IntersectObject} obj
 * @param {number} x
 * @param {number} y
 * @param {CanvasEngine.Coordinates} cursorPost
 * @constructor
 */
CanvasEngine.IntersectResult = function (obj, x, y, cursorPost) {
    /**
     *
     * @type {CanvasEngine.IntersectObject}
     * @private
     */
    this.obj_ = obj;
    /**
     *
     * @type {number}
     * @private
     */
    this.x_ = x;
    /**
     *
     * @type {number}
     * @private
     */
    this.y_ = y;
    /**
     * Дистанция между указателем и объектом
     * @type {number}
     * @private
     */
    this.distance_ = Math.sqrt(Math.pow(this.x_ - cursorPost.getX(), 2) + Math.pow(this.y_ - cursorPost.getY(), 2));
};
/**
 * Получаем дистанцию между указателем и объектом
 * @return {number}
 */
CanvasEngine.IntersectResult.prototype.getDistance = function () {
    return this.distance_;
};
/**
 *
 * @return {CanvasEngine.IntersectObject}
 */
CanvasEngine.IntersectResult.prototype.getObj = function () {
    return this.obj_;
};
/**
 *
 * @return {number}
 */
CanvasEngine.IntersectResult.prototype.getX = function () {
    return this.x_;
};
/**
 *
 * @return {number}
 */
CanvasEngine.IntersectResult.prototype.getFreeX = function () {
    var x = this.x_ >> 0;
    if (x === this.obj_.getX1()) {
        return x - 1;
    }
    if (x === this.obj_.getX2()) {
        return x + 1;
    }
    return x;
};
/**
 *
 * @return {number}
 */
CanvasEngine.IntersectResult.prototype.getFreeY = function () {
    var y = this.y_ >> 0;
    if (y === this.obj_.getY1()) {
        return y - 1;
    }
    if (y === this.obj_.getY2()) {
        return y + 1;
    }
    return y;
};
/**
 *
 * @return {number}
 */
CanvasEngine.IntersectResult.prototype.getY = function () {
    return this.y_;
};
/* global CanvasEngine, goog */
/**
 *
 * @constructor
 */
CanvasEngine.Loader = function () {
    this.resourcesCount_ = 0;
    this.callbacksList_ = {};
    /**
   *
   * @type {Object}
   * @private
   */
    this.returnData_ = {};
};
/**
 *
 * @param {Array.<CanvasEngine.ResLoadType>} resList
 */
CanvasEngine.Loader.prototype.load = function (resList) {
    var that = this;
    this.resourcesCount_ = 0;
    if (!Array.isArray(resList)) {
        throw new Error('The param \'resList\' must be Array');
    }
    if (resList.length === 0) {
        this.checkIsDone();
        return;
    }
    jQuery.each(resList, function (num, obj) {
        if (obj.getData() instanceof CanvasEngine.ResLoadTypeJson) {
            that.resourcesCount_ += 1;
        } else {
            that.resourcesCount_ += Object.keys(obj.getData()).length;
        }
    });
    for (var num = 0; num < resList.length; num += 1) {
        if (resList[num] instanceof CanvasEngine.ResLoadType) {
            this.loadBlock_(resList[num]);
        } else {
            throw new Error('resList must be Array.<ResLoadType>');
        }
    }
};
/**
 *
 * @param {string|number} eventName
 * @param {Function} callback
 */
CanvasEngine.Loader.prototype.addEventListener = function (eventName, callback) {
    this.callbacksList_[eventName] = callback;
};
/**
 *
 */
CanvasEngine.Loader.prototype.checkIsDone = function () {
    if (this.resourcesCount_ === 0) {
        this.callbacksList_[CanvasEngine.Loader.DONE](new CanvasEngine.ResLoadEvent(CanvasEngine.Loader.DONE, this.returnData_));
    }
};
/**
 *
 * @param {string} blockName
 * @param {Object} jsonData
 * @private
 */
CanvasEngine.Loader.prototype.parseJson_ = function (blockName, jsonData) {
    // Object.keys(jsonData.list).length;
    /*for(var name in jsonData) {
   if (!jsonData.hasOwnProperty(name)) {
   continue;
   }

   this.returnData_[blockName][name] = jsonData[name];
   this.loadImage_(blockName, name, jsonData[name].url);
   }*/
    this.returnData_[blockName].sprite = jsonData;
    this.loadImage_(blockName, 'sprite', jsonData.url);
};
/**
 *
 * @param {CanvasEngine.ResLoadType} resLoad
 * @private
 */
CanvasEngine.Loader.prototype.loadBlock_ = function (resLoad) {
    var that = this;
    var resLoadData = resLoad.getData();
    var resLoadName = resLoad.getName();
    this.returnData_[resLoadName] = {};
    if (resLoadData instanceof CanvasEngine.ResLoadTypeJson) {
        jQuery.getJSON(resLoadData.getUrl(), function (data) {
            that.parseJson_(resLoadName, data);
        });
        return;
    }
    for (var imgName in resLoadData) {
        if (!resLoadData.hasOwnProperty(imgName)) {
            continue;
        }
        this.returnData_[resLoadName][imgName] = {};
        this.loadImage_(resLoadName, imgName, resLoadData[imgName]);
    }
};
/**
 *
 * @param {string} groupName
 * @param {string} imgName
 * @param {string} url
 * @private
 */
CanvasEngine.Loader.prototype.loadImage_ = function (groupName, imgName, url) {
    var that = this;
    var img = document.createElement('img');
    img.onload = function () {
        // console.log('that.resourcesCount_ = ', that.resourcesCount_, ' ', url);
        that.returnData_[groupName][imgName].img = this;
        that.resourcesCount_ -= 1;
        that.checkIsDone();
    };
    img.onerror = function () {
        that.resourcesCount_ -= 1;
        that.checkIsDone();
    };
    img.src = url;
};
/**
 * @const {string}
 */
CanvasEngine.Loader.DONE = 'load.done';
/* global CanvasEngine */
/**
 * Map interface
 * @interface
 */
CanvasEngine.MapInterface = function () {
};
/**
 *
 */
CanvasEngine.MapInterface.prototype.getResPoolName = function () {
};
/**
 *
 */
CanvasEngine.MapInterface.prototype.getResPoolList = function () {
};
/**
 *
 */
CanvasEngine.MapInterface.prototype.addResToResPool = function () {
};
/* global CanvasEngine, goog */
/**
 *
 * @param {string} name
 * @param {Object} data
 * @constructor
 */
CanvasEngine.ResLoadEvent = function (name, data) {
    /**
   *
   * @type {string}
   * @private
   */
    this.name_ = name;
    /**
   *
   * @type {Object}
   * @private
   */
    this.data_ = data;
};
/**
 *
 * @return {Object}
 */
CanvasEngine.ResLoadEvent.prototype.getData = function () {
    return this.data_;
};
/**
 *
 * @return {string}
 */
CanvasEngine.ResLoadEvent.prototype.getName = function () {
    return this.name_;
};
/* global CanvasEngine, goog */
/**
 *
 * @param {string} name
 * @param {Object|CanvasEngine.ResLoadTypeJson} data
 * @constructor
 */
CanvasEngine.ResLoadType = function (name, data) {
    if (typeof name !== 'string') {
        throw new Error('CanvasEngine.ResLoadType name must be string');
    }
    /**
   * @private
   * @type {string}
   */
    this.name_ = name;
    /**
   * @private
   * @type {Object|CanvasEngine.ResLoadTypeJson}
   */
    this.data_ = data;
};
/**
 *
 * @return {Object|CanvasEngine.ResLoadTypeJson}
 */
CanvasEngine.ResLoadType.prototype.getData = function () {
    return this.data_;
};
/**
 *
 * @return {string}
 */
CanvasEngine.ResLoadType.prototype.getName = function () {
    return this.name_;
};
/* global CanvasEngine, goog */
/**
 *
 * @param {string} url
 * @constructor
 */
CanvasEngine.ResLoadTypeJson = function (url) {
    /**
   *
   * @type {string}
   * @private
   */
    this.url_ = url;
};
/**
 *
 * @return {string}
 */
CanvasEngine.ResLoadTypeJson.prototype.getUrl = function () {
    return this.url_;
};
/* global CanvasEngine, goog */
/**
 *
 * @param {number} speed Скорость в пикселах за секунду
 * @param {CanvasEngine.GameCanvas} gameCanvas
 * @constructor
 */
CanvasEngine.SpeedCoordination = function (speed, gameCanvas) {
    /**
   * @private
   * @type {number}
   */
    this.speedValue_ = speed;
    this.setSpeedPerSecond(this.speedValue_);
    /**
   *
   * @type {boolean}
   * @private
   */
    this.isPause_ = false;
    var that = this;
    /**
   *
   * @type {?CanvasEngine.GameCanvas}
   * @private
   */
    this.gameCanvas_ = gameCanvas;
    this.gameCanvas_.addReUpdateFpsNotifyCallback(function () {
        that.onReUpdateFpsChangeStatus_();
    });
};
/**
 *
 * @private
 */
CanvasEngine.SpeedCoordination.prototype.onReUpdateFpsChangeStatus_ = function () {
    this.setSpeedPerSecond(this.speedValue_);
};
/**
 *
 */
CanvasEngine.SpeedCoordination.prototype.pause = function () {
    this.isPause_ = true;
};
/**
 *
 * @return {number}
 */
CanvasEngine.SpeedCoordination.prototype.getFullSpeed = function () {
    return this.speedValue_;
};
/**
 *
 * @return {boolean}
 */
CanvasEngine.SpeedCoordination.prototype.isPause = function () {
    return this.isPause_;
};
/**
 *
 */
CanvasEngine.SpeedCoordination.prototype.resume = function () {
    this.isPause_ = false;
};
/**
 * @deprecated Выкидываем, как не нужный
 * @param {number} timestamp
 */
//CanvasEngine.SpeedCoordination.prototype.setTimeStamp = function(timestamp) {
//this.timeStamp_ = timestamp > 1000 ? timestamp - Math.floor(timestamp/1000) * 1000 : timestamp;
//this.timeStamp_ = timestamp;
//console.log(this.timeStamp_);
//};
/**
 *
 * @param {number} speed
 */
CanvasEngine.SpeedCoordination.prototype.setSpeedPerSecond = function (speed) {
    var that = this;
    setTimeout(function () {
        if (CanvasEngine.GameCanvas.TICKS_PER_SECOND < 30) {
            that.speedPerSecond_ = speed / CanvasEngine.GameCanvas.TICKS_PER_SECOND;
        }
    }, 2000);
    this.speedPerSecond_ = speed / CanvasEngine.GameCanvas.TICKS_PER_SECOND;
};
/**
 *
 * @return {number}
 */
CanvasEngine.SpeedCoordination.prototype.getSpeed = function () {
    return this.gameCanvas_.isTick() && !this.isPause_ ? this.speedPerSecond_ : 0;
};
/* global CanvasEngine, goog */
/**
 * @param {?CanvasEngine.GameCanvas=} opt_gameCanvas = null opt_gameCanvas
 * @constructor
 * @implements {CanvasEngine.SpriteInterface}
  */
CanvasEngine.Sprite = function (opt_gameCanvas) {
    /**
   * List of surfaces object
   * @type {Object}
   * @private
   */
    this.surfacesList_ = {};
    /**
   * Current number of surface in surface's list
   * @type {string}
   * @private
   */
    this.currentSurface_ = 'def';
    /**
   * Flag for draw surface or not
   * @type {boolean}
   * @private
   */
    this.isDraw_ = true;
    /**
   * Coordinates
   * @type {CanvasEngine.Coordinates}
   * @private
   */
    this.coordinates_ = new CanvasEngine.Coordinates(0, 0);
    /**
   * Width
   * @type {number}
   * @private
   */
    this.widthCanvas_ = -1;
    /**
   * Height
   * @type {number}
   * @private
   */
    this.heightCanvas_ = -1;
    /**
   * @type {Object}
   * @private
   */
    this.imgInfo_ = {};
    /**
   *
   * @type {Object}
   * @private
   */
    this.animInfo_ = {};
    /**
   * @private
   * @type {number}
   */
    this.animTimer_ = 0;
    /**
   *
   * @type {Array.<string>}
   * @private
   */
    this.animInfoKeyList_ = [];
    /**
   * @private
   * @type {number}
   */
    this.animInfoKeyCurrent_ = 0;
    /**
   *
   * @type {?CanvasEngine.GameCanvas}
   * @private
   */
    this.gameCanvas_ = opt_gameCanvas ? opt_gameCanvas : null;
    /**
   *
   * @type {number}
   * @private
   */
    this.zIndex_ = 0;
};
/**
 * Получаем приоритет слоя
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getZIndex = function () {
    return this.zIndex_;
};
/**
 * Устанавливаем приоритет слоя
 * @param {number} zIndex
 */
CanvasEngine.Sprite.prototype.setZIndex = function (zIndex) {
    this.zIndex_ = zIndex;
};
/**
 *
 * @param {number} speed
 * @return {CanvasEngine.SpeedCoordination}
 */
CanvasEngine.Sprite.prototype.makeSpeedCoordination = function (speed) {
    return new CanvasEngine.SpeedCoordination(speed, this.gameCanvas_);
};
/**
 *
 * @param {CanvasRenderingContext2D} ctx
 * @param {string} animKey
 */
CanvasEngine.Sprite.prototype.drawSlide = function (ctx, animKey) {
    this.drawSlideWithXY(ctx, animKey, this.getCoordinates().getX(), this.getCoordinates().getY());
};
/**
 *
 * @param {CanvasRenderingContext2D} ctx
 * @param {string} animKey
 * @param {number} x
 * @param {number} y
 */
CanvasEngine.Sprite.prototype.drawSlideWithXY = function (ctx, animKey, x, y) {
    ctx.drawImage(this.getSurface(), this.imgInfo_[animKey]['x'], this.imgInfo_[animKey]['y'], this.imgInfo_[animKey]['width'], this.imgInfo_[animKey]['height'], x, y, this.imgInfo_[animKey]['width'], this.imgInfo_[animKey]['height']);
};
/**
 *
 * @param {CanvasRenderingContext2D} ctx
 * @param {HTMLImageElement} img
 * @param {Object} obj
 * @param {number} x
 * @param {number} y
 */
CanvasEngine.Sprite.prototype.drawPartImgWithXY = function (ctx, img, obj, x, y) {
    ctx.drawImage(img, obj.x, obj.y, obj.width, obj.height, x, y, obj.width, obj.height);
};
/**
 *
 * @param {CanvasEngine.GameCanvas} gameCanvas
 */
CanvasEngine.Sprite.prototype.setGameCanvas = function (gameCanvas) {
    this.gameCanvas_ = gameCanvas;
};
/**
 *
 * @return {CanvasEngine.GameCanvas}
 */
CanvasEngine.Sprite.prototype.getGameCanvas = function () {
    return this.gameCanvas_;
};
/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {number} timestamp
 */
CanvasEngine.Sprite.prototype.drawAnimation = function (ctx, timestamp) {
    var animKey = this.animInfoKeyList_[this.animInfoKeyCurrent_];
    this.drawSlide(ctx, animKey);
    if (timestamp - this.animTimer_ > this.animInfo_[animKey].time) {
        this.nextAnimationKey();
        this.animTimer_ = timestamp;
    }
};
/**
 */
CanvasEngine.Sprite.prototype.nextAnimationKey = function () {
    this.animInfoKeyCurrent_ += 1;
    if (this.animInfoKeyCurrent_ === this.animInfoKeyList_.length) {
        this.animInfoKeyCurrent_ = 0;
    }
};
/**
 *
 * @param {!Object} imgInfo
 */
CanvasEngine.Sprite.prototype.setSpriteInfo = function (imgInfo) {
    this.imgInfo_ = imgInfo;
};
/**
 *
 * @param {!Object} imgInfo
 * @param {!Object} animationInfo
 */
CanvasEngine.Sprite.prototype.setAnimationInfo = function (imgInfo, animationInfo) {
    this.imgInfo_ = imgInfo;
    this.animInfo_ = animationInfo;
    this.animInfoKeyList_ = Object.keys(this.animInfo_);
};
/**
 *
 * @param {CanvasEngine.Coordinates} coordinates
 */
CanvasEngine.Sprite.prototype.linkCoordinates = function (coordinates) {
    this.setCoordinates(coordinates);
};
/**
 *
 * @return {CanvasEngine.Coordinates}
 */
CanvasEngine.Sprite.prototype.getCoordinates = function () {
    return this.coordinates_;
};
/**
 *
 * @param {CanvasEngine.Coordinates} coordinates
 */
CanvasEngine.Sprite.prototype.setCoordinates = function (coordinates) {
    this.coordinates_ = coordinates;
};
/**
 *
 * @param {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement} surface
 * @param {string=} opt_key
 *
 * @return {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement}
 */
CanvasEngine.Sprite.prototype.setSurface = function (surface, opt_key) {
    opt_key = opt_key ? opt_key : this.currentSurface_;
    if (!(surface instanceof HTMLImageElement || surface instanceof HTMLCanvasElement || surface instanceof HTMLVideoElement)) {
        throw new Error('Failed to execute \'setSurface\' on \'Sprite\': The provided value is not of type ' + '\'(HTMLImageElement or HTMLVideoElement or HTMLCanvasElement or ImageBitmap)\'');
    }
    this.surfacesList_[opt_key] = surface;
    return surface;
};
/**
 *
 * @param {string} key
 */
CanvasEngine.Sprite.prototype.setCurrentSurfaceKey = function (key) {
    this.currentSurface_ = key;
};
/**
 *
 * @param {string=} opt_name
 * @return {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement}
 */
CanvasEngine.Sprite.prototype.getSurface = function (opt_name) {
    return this.surfacesList_[opt_name ? opt_name : this.currentSurface_];
};
/**
 *
 * @return {boolean}
 */
CanvasEngine.Sprite.prototype.isDraw = function () {
    return this.isDraw_;
};
/**
 *
 */
CanvasEngine.Sprite.prototype.hide = function () {
    this.isDraw_ = false;
};
/**
 *
 */
CanvasEngine.Sprite.prototype.show = function () {
    this.isDraw_ = true;
};
/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {number} timestamp
 */
CanvasEngine.Sprite.prototype.draw = function (ctx, timestamp) {
    throw new Error('You must overload this method: CanvasEngine.Sprite.prototype.draw');
};
/**
 *
 * @param {HTMLImageElement|HTMLCanvasElement} surface
 * @param {string=} opt_name = '' opt_name Название поверхности
 */
CanvasEngine.Sprite.prototype.addSurface = function (surface, opt_name) {
    var name = opt_name ? opt_name : this.currentSurface_;
    this.surfacesList_[name] = surface;
};
/**
 * Уставливаем ширину спрайта
 * @param {number} width Ширина
 */
CanvasEngine.Sprite.prototype.setWidth = function (width) {
    this.widthCanvas_ = width;
};
/**
 * Уставливаем высоту спрайта
 * @param {number} height Высота
 */
CanvasEngine.Sprite.prototype.setHeight = function (height) {
    this.heightCanvas_ = height;
};
/**
 * Ширина спрайта
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getWidth = function () {
    return this.widthCanvas_;
};
/**
 * Высота спрайта
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getHeight = function () {
    return this.heightCanvas_;
};
/**
 * Получаем горизонтальный центр спрайта
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getHorizontalCenter = function () {
    return this.coordinates_.getX() + this.widthCanvas_ / 2 >> 0;
};
/**
 * Получаем вертикальный центр спрайта
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getVerticalCenter = function () {
    return this.coordinates_.getY() + this.heightCanvas_ / 2 >> 0;
};
/**
 *
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getVertTop = function () {
    return this.coordinates_.getY();
};
/**
 *
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getVertBottom = function () {
    return this.coordinates_.getY() + this.heightCanvas_;
};
/**
 *
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getHorizLeft = function () {
    return this.coordinates_.getX();
};
/**
 *
 * @return {number}
 */
CanvasEngine.Sprite.prototype.getHorizRight = function () {
    return this.coordinates_.getX() + this.widthCanvas_;
};
/**
 *
 * @param {number} speed
 */
CanvasEngine.Sprite.prototype.moveHorizontal = function (speed) {
    if (speed === 0) {
        return;
    }
    this.coordinates_.incX(speed);
};
/**
 *
 * @param {number} speed
 */
CanvasEngine.Sprite.prototype.moveVertical = function (speed) {
    if (speed === 0) {
        return;
    }
    this.coordinates_.incY(speed);
};
/* global CanvasEngine */
/**
 * @interface
 */
CanvasEngine.SpriteInterface = function () {
};
/**
 * Координаты отрисовки
 *
 * @return {CanvasEngine.Coordinates} Coordinates
 */
CanvasEngine.SpriteInterface.prototype.getCoordinates = function () {
};
/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {number} timestamp
 */
CanvasEngine.SpriteInterface.prototype.draw = function (ctx, timestamp) {
};
/**
 * Получаем объект для отрисоки
 * @param {string=} opt_name
 * @return {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement}
 */
CanvasEngine.SpriteInterface.prototype.getSurface = function (opt_name) {
};
/**
 * Рисовать ли объект
 *
 * @return {boolean} Draw surface or not
 */
CanvasEngine.SpriteInterface.prototype.isDraw = function () {
};
/**
 *
 * @param {number} index
 */
CanvasEngine.SpriteInterface.prototype.setZIndex = function (index) {
};
/**
 * Получаем приоритет слоя
 * @return {number}
 */
CanvasEngine.SpriteInterface.prototype.getZIndex = function () {
};
/* global CanvasEngine, goog */
/**
 *
 * @param {number} x
 * @param {number} y
 * @param {number} width
 * @param {number} height
 * @constructor
 */
CanvasEngine.Square = function (x, y, width, height) {
    /**
   *
   * @type {number}
   */
    this.x = x;
    /**
   *
   * @type {number}
   */
    this.y = y;
    /**
   *
   * @type {number}
   * @private
   */
    this.width_ = width;
    /**
   *
   * @type {number}
   * @private
   */
    this.height_ = height;
};
/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getWidth = function () {
    return this.width_;
};
/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getHeight = function () {
    return this.height_;
};
/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getX = function () {
    return this.x;
};
/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getY = function () {
    return this.y;
};
/**
 *
 * @param {number} x
 */
CanvasEngine.Square.prototype.setX = function (x) {
    this.x = x;
};
/**
 *
 * @param {number} y
 */
CanvasEngine.Square.prototype.setY = function (y) {
    this.y = y;
};
/**
 *
 * @param {CanvasEngine.ResLoadEvent} resLoadEvent
 * @constructor
 */
function LoaderAddEventListenerCallbackEvent(resLoadEvent) {
}
/* global CanvasEngine */
(function () {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; x += 1) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }
    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function (callback) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () {
                    this.requestAnimationFrame.timerCurrent += new Date().getTime() - currTime;
                    callback(this.requestAnimationFrame.timerCurrent);
                }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }
    window.requestAnimationFrame.timerCurrent = 0;
    window.requestAnimationFrameNew = window.requestAnimationFrame;
    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
    }
    /*jshint ignore:start */
    window['requestAnimationFrame'] = window.requestAnimationFrame;
    window['requestAnimationFrameNew'] = window.requestAnimationFrameNew;
    window['cancelAnimationFrame'] = window.cancelAnimationFrame;    /*jshint ignore:end */
}());
/**
 * Подерживается ли touch screen
 * @return {boolean} true - если да
 */
CanvasEngine.isTouchDevice = function () {
    try {
        document.createEvent('TouchEvent');
        return true;
    } catch (e) {
        return false;
    }
};
/**
 *
 * @const {number}
 */
CanvasEngine.DEVICE_PIXEL_RATIO_100 = 1;
//CanvasEngine.DEVICE_PIXEL_RATIO_150 = 1.5;
/**
 *
 * @const {number}
 */
CanvasEngine.DEVICE_PIXEL_RATIO_200 = 2;
/**
 *
 * @const {number}
 */
CanvasEngine.DEVICE_PIXEL_RATIO_300 = 3;
/**
 *
 * @const {number}
 */
CanvasEngine.DEVICE_PIXEL_RATIO_400 = 4;
/**
 *
 * @return {number}
 */
CanvasEngine.getPixelRation = function () {
    var pixelRatio = window['devicePixelRatio'] || 1;
    if (pixelRatio > 4) {
        return 4;
    }
    if ([1, /*1.5,*/ 2, 3, 4].indexOf(pixelRatio) !== -1) {
        return pixelRatio;
    }
    return Math.round(pixelRatio);
};
/**
 *
 * @param {number} pixelRatio
 * @return {string}
 */
CanvasEngine.getPixelRationPrefix = function (pixelRatio) {
    return pixelRatio + 'x';
};    /*jshint ignore:start */
      // CanvasEngine['isIOs'] = CanvasEngine.isIOs;
      // CanvasEngine['isNativeAndroidBrowser'] = CanvasEngine.isNativeAndroidBrowser;
      // CanvasEngine['isAndroidBrowser'] = CanvasEngine.isAndroidBrowser;
      // CanvasEngine['getPixelRationPrefix'] = CanvasEngine.getPixelRationPrefix;
      // CanvasEngine['getPixelRation'] = CanvasEngine.getPixelRation;
      // CanvasEngine['isTouchDevice'] = CanvasEngine.isTouchDevice;
      // window['CanvasEngine']['DEVICE_PIXEL_RATIO_100'] = CanvasEngine.DEVICE_PIXEL_RATIO_100;
      // window['CanvasEngine']['DEVICE_PIXEL_RATIO_200'] = CanvasEngine.DEVICE_PIXEL_RATIO_200;
      // window['CanvasEngine']['DEVICE_PIXEL_RATIO_300'] = CanvasEngine.DEVICE_PIXEL_RATIO_300;
      // window['CanvasEngine']['DEVICE_PIXEL_RATIO_400'] = CanvasEngine.DEVICE_PIXEL_RATIO_400;
      /*jshint ignore:end */