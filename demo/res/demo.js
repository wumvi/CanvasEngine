'use strict';
/* global Demon, CanvasEngine, EventDomDispatcher */



/**
 * @param {Demon.MapBase} map
 * @param {Demon.SizeInfo} sizeInfo
 * @param {string} subCategoryEventName
 * @param {HTMLCanvasElement} canvasDomElement
 * @param {string} url
 * @constructor
 */
Demon.GameController = function(map, sizeInfo, subCategoryEventName, canvasDomElement, url) {
  var that = this;

  /**
   *
   * @type {Demon.ResModel}
   * @private
   */
  this.loadRes_ = new Demon.ResModel(url, sizeInfo);
  this.fillResLoad_(this.loadRes_, this.map_);

  /**
   *
   * @type {HTMLCanvasElement}
   * @private
   */
  this.canvasDomElement_ = canvasDomElement;

  if (!(this.canvasDomElement_ instanceof HTMLCanvasElement)) {
    throw new Error('Element obj not is HTMLCanvasElement');
  }

  this.canvasDomElement_.width = this.width_;
  this.canvasDomElement_.height = this.height_;

  /**
   * Количество правильно открытых пар-карт
   * @type {number}
   * @private
   */
  this.countOfOpenedTile_ = 0;

  /**
   *
   * @type {!jQuery}
   * @private
   */
  this.$root_ = jQuery(canvasDomElement);

  /**
   * @private
   * @type {CanvasRenderingContext2D}
   */
  this.ctxMain_ = /** @type {CanvasRenderingContext2D} */ (this.canvasDomElement_.getContext('2d'));

  /**
   *
   * @type {CanvasEngine.GameCanvas}
   * @private
   */
  this.gameCanvas_ = new CanvasEngine.GameCanvas(this.width_, this.height_);

  var gameCtx = this.gameCanvas_.getContext();
  gameCtx.font = this.sizeInfo_.getFontSize() + 'px Varela Round';
  gameCtx.textBaseline = 'middle';
  gameCtx.textAlign = 'center';

  /**
   *
   * @type {Array.<Demon.Draw.Tile>}
   * @private
   */
  this.tileDrawList_ = [];

  this.initDrawSprite_();

  /**
   *
   * @type {boolean}
   * @private
   */
  this.isCanClick_ = true;

  /**
   *
   * @type {EventDomDispatcher}
   * @private
   */
  this.eventDomDispatcher_ = new EventDomDispatcher(Demon.EVENT_NAME, subCategoryEventName);

  /**
   *
   * @type {Array.<Demon.WordModel>}
   * @private
   */
  this.tileIdList_ = [];

  /**
   *
   * @type {Demon.DemonModeInterface}
   * @private
   */
  this.demonCurrent_ = null;


  /**
   *
   * @type {CanvasEngine.Loader}
   * @private
   */
  this.loader_ = new CanvasEngine.Loader();

  /**
   *
   * @type {Object}
   * @private
   */
  this.loaderData_ = {};

  this.loader_.addEventListener(CanvasEngine.Loader.DONE, function(eventData) {
    that.loaderData_ = eventData.getData();
    that.init_();
  });

  this.loader_.load(this.loadRes_.getResLoadType());
};


/**
 * @private
 */
Demon.GameController.prototype.initDrawSprite_ = function() {
  var gameCtx = this.gameCanvas_.getContext();

  for (var col = 0; col < this.map_.getRow(); col += 1) {
    for (var row = 0; row < this.map_.getCol(); row += 1) {

      var x = row * (this.tileSize_ + this.borderSize_);
      var y = col * (this.tileSize_ + this.borderSize_);

      var tileDraw = new Demon.Draw.Tile(this.gameCanvas_, this.tileSize_, x, y);
      tileDraw.setContextInfo(gameCtx);
      this.tileDrawList_.push(tileDraw);
      this.gameCanvas_.addSurface(tileDraw);
    }
  }
};


/**
 * Возвращает весь список квадратов
 * @return {Array.<Demon.Draw.Tile>}
 */
Demon.GameController.prototype.getTileList = function() {
  return this.tileDrawList_;
};


/**
 * Вылечиваем все паразитированные ячейки
 */
Demon.GameController.prototype.cureTile = function() {
  for (var num = 0; num < this.tileDrawList_.length; num += 1) {
    this.tileDrawList_[num].setDemonLogic(null);
  }
};


/**
 * @param {boolean} flag
 */
Demon.GameController.prototype.setIsCanClickFlag = function(flag) {
  this.isCanClick_ = flag;
};


/**
 * Устанавливает паузу в игре
 */
Demon.GameController.prototype.pauseGame = function() {
  this.isGamePause_ = true;
};


/**
 * Продолжает игру, если была пауза
 */
Demon.GameController.prototype.resumeGame = function() {
  this.isGamePause_ = false;
  this.rePaint_(0);
};


/**
 * Передаём управление демону, чтобы он мог начать паразитировать
 */
Demon.GameController.prototype.parasiteTile = function() {
  for (var num = 0; num < this.tileDrawList_.length; num += 1) {
    if (!this.tileDrawList_[num].isCanParasite()) {
      continue;
    }

    this.tileDrawList_[num].setDemonLogic(this.demonCurrent_);
  }

  this.demonCurrent_.reset();
  this.demonCurrent_.start();
};


/**
 * Показывать ли подсказки
 */
Demon.GameController.prototype.showTip = function() {
  var that = this;
  this.setIsCanClickFlag(false);
  this.showTipFlag_(true);

  setTimeout(function() {
    that.showTipFlag_(false);
    that.setIsCanClickFlag(true);
    that.demonCurrent_.setUsedDemonMode();
  }, 3000);
};


/**
 * @param {boolean} flag
 * @private
 */
Demon.GameController.prototype.showTipFlag_ = function(flag) {
  for (var num = 0; num < this.tileDrawList_.length; num += 1) {
    if (!this.tileDrawList_[num].isCanShowTipMode()) {
      continue;
    }

    this.tileDrawList_[num].setShowTipFlag(flag);
  }
};


/**
 *
 * @private
 */
Demon.GameController.prototype.init_ = function() {
  this.initEvent_();
};


/**
 *
 * @private
 */
Demon.GameController.prototype.initEvent_ = function() {
  var that = this;

  if (CanvasEngine.isTouchDevice()) {
    this.$root_.on('touchstart', function(event) {
      var clickEventBridge = new CanvasEngine.ClickEventBridge(event, that.canvasDomElement_);
      that.onClick_(
        clickEventBridge.getX() * that.sizeInfo_.getPixelRatio(),
        clickEventBridge.getY() * that.sizeInfo_.getPixelRatio()
      );
      return false;
    });
  } else {
    this.$root_.click(function(event) {
      var clickEventBridge = new CanvasEngine.ClickEventBridge(event);
      that.onClick_(
        clickEventBridge.getX() * that.sizeInfo_.getPixelRatio(),
        clickEventBridge.getY() * that.sizeInfo_.getPixelRatio()
      );
      return false;
    });
  }

  this.rePaint_(0);
};


/**
 *
 * @param {number} mouseX
 * @param {number} mouseY
 * @return {number}
 * @private
 */
Demon.GameController.prototype.getTailNum_ = function(mouseX, mouseY) {
  var posX = Math.floor(mouseX / (this.tileSize_ + this.borderSize_));
  var posY = Math.floor(mouseY / (this.tileSize_ + this.borderSize_));

  return posX + posY * this.map_.getCol();
};


/**
 *
 * @param {Demon.ResModel} loadRes
 * @param {Demon.MapBase} map
 * @private
 */
Demon.GameController.prototype.fillResLoad_ = function(loadRes, map) {
  var wordInfoList = map.getWordInfoList();
  for (var num = 0; num < wordInfoList.length; num += 1) {
    var demonId = wordInfoList[num].getId();
    if (wordInfoList[num].getDemonId()) {
      loadRes.addDemon(demonId);
    }
  }
};


/**
 *
 * @param {string} demonKey
 * @return {?Demon.DemonModeInterface}
 * @private
 */
Demon.GameController.prototype.makeDemonLogic_ = function(demonKey) {
  switch (demonKey) {
    case Demon.Demon.Ouroboros.ID_KEY:
      var res = new Demon.Demon.OuroborosRes(this.loaderData_[Demon.Demon.Ouroboros.ID_KEY]);
      return new Demon.Demon.Ouroboros(res, this);
  }

  return null;
};


/**
 *
 * @param {number} mouseX
 * @param {number} mouseY
 * @private
 */
Demon.GameController.prototype.onClick_ = function(mouseX, mouseY) {
  var num = this.getTailNum_(mouseX, mouseY);
  var tileDraw = this.tileDrawList_[num];

  if (tileDraw.isOpen() || !this.isCanClick_ || this.isGamePause_) {
    return;
  }

  var wordInfo = tileDraw.getWordInfo();

  this.eventDomDispatcher_.emit(
    Demon.EVENT_TILE_CLICK,
    new Demon.TileClickEvent(wordInfo)
  );

  if (wordInfo === null) {
    wordInfo = this.map_.getNextWordInfo();
    tileDraw.setWordInfo(wordInfo);
    // wordInfo.setTileDraw(tileDraw);
    // wordInfo.setDemonDraw(this.makeDemonDraw_(wordInfo.getId()));
    if (this.map_.onCallbackTileDrawSet) {
      this.map_.onCallbackTileDrawSet(tileDraw);
    }

    wordInfo.setDemonLogic(this.makeDemonLogic_(wordInfo.getId()));
  }

  tileDraw.open(mouseX, mouseY);

  // Если в ячейки хранится демон
  if (wordInfo.getDemonId()) {
    // Закрываем другие ячейки
    this.initTimerToCloseWrongTiles_();
    // Запрещаем взаимодействие с доской
    this.setIsCanClickFlag(false);

    // Получаем логику демона
    this.demonCurrent_ = wordInfo.getDemonLogic();

    // Посылаем сообщение в эфир, что демон открылся
    this.eventDomDispatcher_.emit(
      Demon.EVENT_DEMON_SHOW,
      new Demon.Demon.DemonEvent(wordInfo)
    );
    return;
  }

  if (this.wrongTimeoutHandle_) {
    clearTimeout(this.wrongTimeoutHandle_);
    this.wrongTimeoutHandle_ = null;
    this.closeWrongTiles_();
  }

  // Добавляем открытую ячейки в список
  this.tileIdList_.push(wordInfo);

  // Если мы открыли достаточно ячеек для проверки
  if (this.tileIdList_.length === this.map_.getAnswerCount()) {
    // Проверяем правильный ли ответ содержится в ячейках
    var isRight = this.isRightTileAnswer_();
    if (isRight) {
      this.wasOpenRightTile_();
    } else {
      this.wasOpenWrongTile_();
    }
  }
};


/**
 *
 * @private
 */
Demon.GameController.prototype.wasOpenRightTile_ = function() {
  this.tileIdListFree_();
  this.setIsCanClickFlag(true);
  this.countOfOpenedTile_ += 1;

  if (this.countOfOpenedTile_ === this.map_.getTitlePairCount()) {
    this.eventDomDispatcher_.emit(Demon.EVENT_END_GAME, {});
  }
};


/**
 *
 * @private
 */
Demon.GameController.prototype.wasOpenWrongTile_ = function() {
  this.initTimerToCloseWrongTiles_();
};


/**
 *
 * @private
 */
Demon.GameController.prototype.tileIdListFree_ = function() {
  this.tileIdList_ = [];
};


/**
 * Время отображения квадратов открытыми
 * @const {number}
 */
Demon.GameController.SHOW_OPEN_TILE_TIME = 1500;


/**
 * @private
 */
Demon.GameController.prototype.initTimerToCloseWrongTiles_ = function() {
  var that = this;

  this.wrongTimeoutHandle_ = setTimeout(function() {
    that.closeWrongTiles_();
  }, Demon.GameController.SHOW_OPEN_TILE_TIME);
};


/**
 *
 * @private
 */
Demon.GameController.prototype.closeWrongTiles_ = function() {
  for (var num = 0; num < this.tileIdList_.length; num += 1) {
    this.tileIdList_[num].getTileDraw().close();
  }

  this.tileIdListFree_();
};


/**
 *
 * @return {boolean}
 * @private
 */
Demon.GameController.prototype.isRightTileAnswer_ = function() {
  if (this.map_.cbCheckRightAnswer) {
    return this.map_.cbCheckRightAnswer(this.tileIdList_);
  }

  var tileId = this.tileIdList_[0].getId();
  var flag = true;
  for (var num = 1; num < this.tileIdList_.length; num += 1) {
    if (this.tileIdList_[num].getId() !== tileId) {
      flag = false;
      break;
    }
  }

  return flag;
};


/**
 * @return {number}
 */
Demon.GameController.prototype.getWidth = function() {
  return this.canvasDomElement_.width;
};


/**
 * @private
 * @param {number} timestamp
 */
Demon.GameController.prototype.rePaint_ = function(timestamp) {
  var that = this;

  if (this.isGamePause_) {
    return;
  }

  this.gameCanvas_.repaint(timestamp);

  //this.ctxMain_.clearRect(0, 0, this.width_, this.height_);
  this.ctxMain_.fillStyle = '#a5b1b7';
  this.ctxMain_.fillRect(0, 0, this.width_, this.height_);
  this.ctxMain_.drawImage(this.gameCanvas_.getCanvas(), 0, 0);

  window.requestAnimationFrameNew(function(timestamp) {
    that.rePaint_(timestamp);
  }, 1000 / 60);
};

/* jshint ignore:start */
window['Demon']['GameController'] = Demon.GameController;
Demon.GameController.prototype['showTip'] = Demon.GameController.prototype.showTip;
Demon.GameController.prototype['parasiteTile'] = Demon.GameController.prototype.parasiteTile;
Demon.GameController.prototype['cureTile'] = Demon.GameController.prototype.cureTile;
Demon.GameController.prototype['getWidth'] = Demon.GameController.prototype.getWidth;
Demon.GameController.prototype['resumeGame'] = Demon.GameController.prototype.resumeGame;
Demon.GameController.prototype['pauseGame'] = Demon.GameController.prototype.pauseGame;
/* jshint ignore:end */
