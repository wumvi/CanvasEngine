'use strict';
/* global CanvasEngine, goog */



/**
 *
 * @constructor
 */
function Controller() {

}



/**
 *
 * @param {CanvasEngine.GameCanvas} gameCanvas
 * @extends {CanvasEngine.Sprite}
 * @constructor
 */
function MySprite(gameCanvas) {
  CanvasEngine.Sprite.call(this, gameCanvas);
  //console.log(CanvasEngine.Sprite);
}
goog.inherits(MySprite, CanvasEngine.Sprite);


/**
 * Главный метод отрисовки
 * @param {CanvasRenderingContext2D} ctx
 * @param {number} timestamp
 * @override
 */
MySprite.prototype.draw = function(ctx, timestamp) {
  var coordinates = this.getCoordinates();
  ctx.fillStyle = '#78c484';
  ctx.fillRect(0, 0, this.getWidth(), this.getHeight());
};


/**
 *
 * @return {CanvasEngine.Coordinates}
 */
MySprite.prototype.getParentCoordinates = function() {
  return this.getCoordinates();
};

