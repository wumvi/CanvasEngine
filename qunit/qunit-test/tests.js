'use strict';

/* global QUnit, CanvasEngine, EventDomDispatcher, MySprite */


/**
 *
 * @type {number}
 */
QUnit.config.testTimeout = 500;


//QUnit.test('Init Canvas', function(assert) {
//
//});


/**
 * Тестируем создание объекта
 */
QUnit.test('CanvasEngine.GameCanvas base', function (assert) {
    var testFlag = true;
    var width = 150;
    var height = 200;
    var gameCanvas = new CanvasEngine.GameCanvas(width, height);

    assert.ok(gameCanvas.getWidth() === width, 'GameCanvas.getWidth');
    assert.ok(gameCanvas.getHeight() === height, 'GameCanvas.getHeight');

    var canvas = gameCanvas.getCanvas();
    assert.ok(canvas instanceof HTMLCanvasElement, 'Canvas is ok');
    assert.ok(canvas.height === height, 'Canvas height');
    assert.ok(canvas.width === width, 'Canvas width');

    var ctx = gameCanvas.getContext();
    assert.ok(ctx instanceof CanvasRenderingContext2D, 'Context is ok');
    assert.ok(ctx === canvas.getContext('2d'), 'Context is equal');

    assert.throws(function () {
            gameCanvas.addSurface(3);
        },
        'Add bad sprite'
    );

    var spriteItem1 = new MySprite(gameCanvas);
    // spriteItem1.setCoordinates(new CanvasEngine.Coordinates(27, 87));
    spriteItem1.setZIndex(2);
    var spriteItem2 = new MySprite(gameCanvas);
    spriteItem2.setZIndex(3);
    //var zIndexValue = gameCanvas.sortByZIndexCallback_(spriteItem1, spriteItem2);
    //assert.ok(zIndexValue === 1, 'sortByZIndexCallback is ok');

    gameCanvas.addSurface(spriteItem2);
    assert.ok(gameCanvas.getSurfaceList()[0] === spriteItem2, 'Sprites2 equal');
    gameCanvas.addSurface(spriteItem1);
    assert.ok(gameCanvas.getSurfaceList()[0] === spriteItem2, 'Sprites1 equal');

    assert.ok(gameCanvas.getSurfaceList().length === 2, 'addSurface is ok');

    gameCanvas.sortByZIndex();
    var surfaceList = gameCanvas.getSurfaceList();
    testFlag = surfaceList[0] === spriteItem1 && surfaceList[1] === spriteItem2;
    assert.ok(testFlag, 'sortByZIndex is ok');

    width = 60;
    height = 70;
    spriteItem2.setWidth(width);
    spriteItem2.setHeight(height);
    assert.ok(spriteItem2.getWidth() === width, 'Sprite getWidth');
    assert.ok(spriteItem2.getHeight() === height, 'Sprite getHeight');
    assert.ok(spriteItem2.isDraw(), 'Sprite isDraw');

    var globalCanvas = document.getElementById('canv');
    assert.ok(globalCanvas instanceof HTMLCanvasElement, 'Global Canvas is ok');

    assert.ok(globalCanvas.getContent === undefined, 'Global HTML Canvas support');

    var globalCtx = globalCanvas.getContext('2d');
    assert.ok(globalCtx instanceof CanvasRenderingContext2D, 'Global Context is ok');

    // todo Вставить пиксельную проверку
    //spriteItem2.draw(globalCtx, 0);


    gameCanvas.repaint(0);
    globalCtx.drawImage(gameCanvas.getCanvas(), 0, 0);
});


QUnit.test('CanvasEngine.GameCanvas TickCallback', function (assert) {
    var gameCanvas = new CanvasEngine.GameCanvas(20, 23);

    assert.expect(2);
    var done1 = assert.async();

    assert.ok(!gameCanvas.isTick(), 'isTick begin is false');

    gameCanvas.addTickCallback(function () {
        assert.ok(gameCanvas.isTick(), 'isTick begin is true');
        done1();
    });

    gameCanvas.repaint(1000);
});


QUnit.test('Sprite', function (assert) {
    var width = 150;
    var height = 200;
    var gameCanvas = new CanvasEngine.GameCanvas(width, height);
    var spriteItem = new MySprite(gameCanvas);

    var zIndex = 5;
    spriteItem.setZIndex(zIndex);
    assert.ok(spriteItem.getZIndex() === zIndex, 'zIndex is ok');

    var speedCoordination = spriteItem.makeSpeedCoordination(300);
    assert.ok(speedCoordination instanceof CanvasEngine.SpeedCoordination, 'makeSpeedCoordination is ok');

    // todo test drawSlide

    // todo test drawSlideWithXY

    // todo test drawPartImgWithXY

    var spriteItem2 = new MySprite();
    assert.ok(spriteItem2.getGameCanvas() === null, 'getGameCanvas is ok');
    spriteItem2.setGameCanvas(gameCanvas);
    assert.ok(spriteItem2.getGameCanvas() === gameCanvas, 'setGameCanvas is ok');

    // todo test nextAnimationKey
    // todo test setSpriteInfo
    // todo test setAnimationInfo
    // todo test linkCoordinates
    // todo test getCoordinates
    // todo test setCoordinates
    // todo test setSurface
    // todo test setCurrentSurfaceKey
    // todo test getSurface
    // todo test isDraw

    var coordinates = spriteItem.getParentCoordinates();

});


QUnit.test('CanvasEngine.Coordinates', function (assert) {
    var x = 20;
    var y = 30;
    var coordinates = new CanvasEngine.Coordinates(x, y);
    assert.ok(coordinates.getX() === x && coordinates.getY() === y, 'getX and getY is ok');

    x = 40;
    y = 50;
    coordinates.setX(x);
    coordinates.setY(y);
    assert.ok(coordinates.getX() === x && coordinates.getY() === y, 'setX and setY is ok');

    x = 70;
    y = 80;
    coordinates.setXY(x, y);
    assert.ok(coordinates.getX() === x && coordinates.getY() === y, 'setXY is ok');

    x = 100;
    y = 200;
    var inc = 10;
    coordinates.setXY(x, y);
    coordinates.incX(inc);
    coordinates.incY(inc);
    assert.ok(coordinates.getX() === (x + inc) && coordinates.getY() === (y + inc), 'incX and incX is ok');
});

QUnit.test('CanvasEngine.Intersect', function (assert) {

    var obj, list;
    var intersectController = new CanvasEngine.Intersect();
    /* intersectController.addObject(new CanvasEngine.IntersectObject(10, 20, 0, 50));

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0, 0),
        new CanvasEngine.Coordinates(5, 5)
    );
    assert.ok(list.length === 0, 'Left before');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(25, 25),
        new CanvasEngine.Coordinates(35, 35)
    );
    assert.ok(list.length === 0, 'Left after');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(5, 5),
        new CanvasEngine.Coordinates(0, 0)
    );
    assert.ok(list.length === 0, 'Left before revers');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(35, 35),
        new CanvasEngine.Coordinates(25, 25)
    );
    assert.ok(list.length === 0, 'Left after revers');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0, 0),
        new CanvasEngine.Coordinates(30, 30)
    );
    assert.ok(list.length === 1, 'Left intersection');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0, 0),
        new CanvasEngine.Coordinates(15, 15)
    );
    assert.ok(list.length === 1, 'Left intersection in area');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0, 0),
        new CanvasEngine.Coordinates(10, 10)
    );
    assert.ok(list.length === 1, 'Left intersection on border');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0, 10),
        new CanvasEngine.Coordinates(50, 10)
    );
    assert.ok(list.length === 1, 'Y=fix');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(50, 10),
        new CanvasEngine.Coordinates(0, 10)
    );
    assert.ok(list.length === 1, 'Y=fix revers');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0, 10),
        new CanvasEngine.Coordinates(5, 10)
    );
    assert.ok(list.length === 0, 'Y=fix nointeraction before');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(5, 10),
        new CanvasEngine.Coordinates(0, 10)
    );
    assert.ok(list.length === 0, 'Y=fix nointeraction before revers');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(25, 10),
        new CanvasEngine.Coordinates(30, 10)
    );
    assert.ok(list.length === 0, 'Y=fix nointeraction after');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(30, 10),
        new CanvasEngine.Coordinates(25, 10)
    );
    assert.ok(list.length === 0, 'Y=fix nointeraction after revers');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0, 10),
        new CanvasEngine.Coordinates(10, 10)
    );
    obj = list[0];
    assert.ok(obj.getFreeX() === 9 && obj.getFreeY() === 10, 'Left free point');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(50, 50),
        new CanvasEngine.Coordinates(0, 0)
    );
    obj = list[0];
    assert.ok(obj.getFreeX() === 21 && obj.getFreeY() === 20, 'Right free point');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(15, -5),
        new CanvasEngine.Coordinates(15, 30)
    );
    obj = list[0];
    assert.ok(obj.getFreeX() === 15 && obj.getFreeY() === -1, 'Bottom free point');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(15, 60),
        new CanvasEngine.Coordinates(15, 45)
    );
    obj = list[0];
    assert.ok(obj.getFreeX() === 15 && obj.getFreeY() === 51, 'Top free point');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0, 10),
        new CanvasEngine.Coordinates(20, -10)
    );
    obj = list[0];
    assert.ok(obj.getFreeX() === 9 && obj.getFreeY() === -1, 'Center left free point');*/

    intersectController.clearObjectList();
    intersectController.addObject(new CanvasEngine.IntersectObject(50, 90, 30, 100));
    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(70, 0),
        new CanvasEngine.Coordinates(69, 76)
    );
    console.log(list);





    /*var lastPosCursor = new CanvasEngine.Coordinates(0, 0);
    var currentPosCursor = new CanvasEngine.Coordinates(3, 3);

    var intersectController = new CanvasEngine.Intersect();
    var obj = new CanvasEngine.IntersectObject(1, 2, 0, 4);
    intersectController.addObject(obj);

    var list = intersectController.getIntersectObjectList(lastPosCursor, currentPosCursor);
    var result = list[0];
    assert.ok(result.getX() === 1 && result.getY() === 1, 'X=Y up');
    assert.ok(result.getObj() === obj, 'Obj is Ok');

    lastPosCursor = new CanvasEngine.Coordinates(5, 5);
    currentPosCursor = new CanvasEngine.Coordinates(0, 0);
    list = intersectController.getIntersectObjectList(lastPosCursor, currentPosCursor);
    result = list[0];
    assert.ok(result.getX() === obj.getX2() && result.getY() === 2, 'X=Y down');

    lastPosCursor = new CanvasEngine.Coordinates(1.5, -1);
    currentPosCursor = new CanvasEngine.Coordinates(1.5, 5);
    list = intersectController.getIntersectObjectList(lastPosCursor, currentPosCursor);
    result = list[0];
    assert.ok(result.getX() === lastPosCursor.getX() && result.getY() === 0, 'X=1.5 up');

    lastPosCursor = new CanvasEngine.Coordinates(1.5, 5);
    currentPosCursor = new CanvasEngine.Coordinates(1.5, -1);
    list = intersectController.getIntersectObjectList(lastPosCursor, currentPosCursor);
    result = list[0];
    assert.ok(result.getX() === lastPosCursor.getX() && result.getY() === obj.getY2(), 'X=1.5 down');

    lastPosCursor = new CanvasEngine.Coordinates(0, 4);
    currentPosCursor = new CanvasEngine.Coordinates(4, 4);
    list = intersectController.getIntersectObjectList(lastPosCursor, currentPosCursor);
    result = list[0];
    assert.ok(result.getX() === obj.getX1() && result.getY() === lastPosCursor.getY(), 'Y=4 left-right');


    lastPosCursor = new CanvasEngine.Coordinates(4, 4);
    currentPosCursor = new CanvasEngine.Coordinates(-1, 4);
    list = intersectController.getIntersectObjectList(lastPosCursor, currentPosCursor);
    result = list[0];
    assert.ok(result.getX() === obj.getX2() && result.getY() === lastPosCursor.getY(), 'Y=4 right-left');

    obj.setEnable(false);
    list = intersectController.getIntersectObjectList(lastPosCursor, currentPosCursor);
    assert.ok(list.length === 0, 'Disable work');
    intersectController.clearObjectList();

    var obj1 = new CanvasEngine.IntersectObject(4, 5, 0, 1);
    intersectController.addObject(obj1);

    lastPosCursor = new CanvasEngine.Coordinates(0, 0);
    currentPosCursor = new CanvasEngine.Coordinates(5, 5);
    list = intersectController.getIntersectObjectList(lastPosCursor, currentPosCursor);
    assert.ok(list.length === 0, 'Object not in area work');
    intersectController.clearObjectList();

    var obj2 = new CanvasEngine.IntersectObject(1, 2, 0, 3);
    intersectController.addObject(obj2);

    lastPosCursor = new CanvasEngine.Coordinates(0, 0);
    currentPosCursor = new CanvasEngine.Coordinates(1.5, 1.5);
    list = intersectController.getIntersectObjectList(lastPosCursor, currentPosCursor);
    result = list[0];
    assert.ok(result.getObj().isPointInObject(currentPosCursor), 'Cursor in');

    currentPosCursor = new CanvasEngine.Coordinates(8, 8);
    assert.ok(!result.getObj().isPointInObject(currentPosCursor), 'Cursor out');
    intersectController.clearObjectList();

    intersectController.addObject(new CanvasEngine.IntersectObject(1, 2, 0, 4));
    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(1.5, 1.5),
        new CanvasEngine.Coordinates(10, 10)
    );
    assert.ok(list.length === 0, 'Line out of bound 1');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0.5, 0.5),
        new CanvasEngine.Coordinates(0, 0)
    );
    assert.ok(list.length === 0, 'Line out of bound 2');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(1.5, -0.5),
        new CanvasEngine.Coordinates(1.5, -1)
    );
    assert.ok(list.length === 0, 'Line out of bound 3');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(2.5, 3),
        new CanvasEngine.Coordinates(3, 3)
    );
    assert.ok(list.length === 0, 'Line out of bound 4');
    intersectController.clearObjectList();

    intersectController.addObject(new CanvasEngine.IntersectObject(3, 4, 0, 8));
    intersectController.addObject(new CanvasEngine.IntersectObject(5, 6, 0, 4));

    var obj5 = new CanvasEngine.IntersectObject(1, 2, 0, 4);
    intersectController.addObject(obj5);


    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0, 0),
        new CanvasEngine.Coordinates(10, 10)
    );
    assert.ok(list.length === 2, 'Two object intersection');
    assert.ok(list[0].getObj() === obj5, 'Sort object');

    intersectController.clearObjectList();
    intersectController.addObject(new CanvasEngine.IntersectObject(1, 2, 0, 5));

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(10, 10),
        new CanvasEngine.Coordinates(5.5, 5.5)
    );
    assert.ok(list.length === 0, 'Line out of bound 5');

    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0, 0),
        new CanvasEngine.Coordinates(0.5, 0.5)
    );
    assert.ok(list.length === 0, 'Line out of bound 6');


    list = intersectController.getIntersectObjectList(
        new CanvasEngine.Coordinates(0, 5),
        new CanvasEngine.Coordinates(0.5, 4.5)
    );
    assert.ok(list.length === 0, 'Line out of bound 7');*/
});


QUnit.test('CanvasEngine.IntersectObject', function (assert) {
    var obj = new CanvasEngine.IntersectObject(1, 2, 0, 4);
    assert.ok(obj.isPointInObject(new CanvasEngine.Coordinates(1.5, 1.5)), 'In area');
    assert.ok(!obj.isPointInObject(new CanvasEngine.Coordinates(0, 1.5)), 'Not in area');
});




