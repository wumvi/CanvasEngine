'use strict';

/* global CanvasEngine */

/**
 *
 * @param {number} x
 * @param {number} y
 * @constructor
 */
CanvasEngine.Coordinates = function(x, y) {
};

CanvasEngine.Coordinates.prototype.getX = function(){
};

CanvasEngine.Coordinates.prototype.getY = function(){
};

CanvasEngine.Coordinates.prototype.setX = function(x){
};

CanvasEngine.Coordinates.prototype.setY = function(y){
};

/**
 * Устанавливаем сразу координату X и Y
 * @param {number} x
 * @param {number} y
 */
CanvasEngine.Coordinates.prototype.setXY = function(x, y) {
};

/**
 * Увеличиваем координату X на величину val
 * @param {number} val
 */
CanvasEngine.Coordinates.prototype.incX = function(val) {
};


/**
 * Увеличиваем координату Y на величину val
 * @param {number} val
 */
CanvasEngine.Coordinates.prototype.incY = function(val) {
};