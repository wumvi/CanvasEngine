'use strict';

/* global CanvasEngine */

/**
 * Map interface
 * @interface
 */
CanvasEngine.MapInterface = function() {

};

CanvasEngine.MapInterface.prototype.getResPoolName = function () {

};

CanvasEngine.MapInterface.prototype.getResPoolList = function () {

};

CanvasEngine.MapInterface.prototype.addResToResPool = function () {

};
