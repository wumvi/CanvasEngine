'use strict';

/* global CanvasEngine */

/**
 * @interface
 */
CanvasEngine.SpriteInterface = function() {
};

/**
 * Координаты отрисовки
 *
 * @returns {CanvasEngine.Coordinates} Coordinates
 */
CanvasEngine.SpriteInterface.prototype.getCoordinates = function () {
};

/**
 * Получаем объект для отрисоки
 * @param {string=} name
 */
CanvasEngine.SpriteInterface.prototype.getSurface = function (name) {
};

/**
 * Рисовать ли объект
 *
 * @returns {boolean} Draw surface or not
 */
CanvasEngine.SpriteInterface.prototype.isDraw = function () {
};