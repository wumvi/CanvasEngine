'use strict';

/* global CanvasEngine */

/**
 *
 * @constructor
 */
CanvasEngine.Loader = function () {
};

/**
 *
 * @param {Array.<CanvasEngine.ResLoadType>} resList
 */
CanvasEngine.Loader.prototype.load = function (resList) {
};

/**
 *
 * @param {string} eventName
 * @param {function(CanvasEngine.ResLoadEvent):void} callback
 */
CanvasEngine.Loader.prototype.addEventListener = function (eventName, callback) {
};

CanvasEngine.Loader.prototype.checkIsDone = function () {
};
