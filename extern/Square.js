'use strict';
/* global CanvasEngine */



/**
 *
 * @param {number} x
 * @param {number} y
 * @param {number} width
 * @param {number} height
 * @constructor
 */
CanvasEngine.Square = function(x, y, width, height) {

};


/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getWidth = function() {
};


/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getHeight = function() {
};


/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getX = function() {
};


/**
 *
 * @return {number}
 */
CanvasEngine.Square.prototype.getY = function() {
};


/**
 *
 * @param {number} x
 */
CanvasEngine.Square.prototype.setX = function(x) {
};


/**
 *
 * @param {number} y
 */
CanvasEngine.Square.prototype.setY = function(y) {
};
