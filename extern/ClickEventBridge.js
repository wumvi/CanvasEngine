'use strict';

/* global CanvasEngine */

/**
 *
 * @param {Object} event
 * @param {Object=} mainCanvasOffset
 * @constructor
 */
CanvasEngine.ClickEventBridge = function(event, mainCanvasOffset) {
};

/**
 *
 * @returns {number}
 */
CanvasEngine.ClickEventBridge.prototype.getX = function(){
};

/**
 *
 * @returns {number}
 */
CanvasEngine.ClickEventBridge.prototype.getY = function(){
};
