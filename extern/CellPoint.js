'use strict';

/* global CanvasEngine */

/**
 *
 * @param {number} x
 * @param {number} y
 * @constructor
 */
CanvasEngine.CellPoint = function(x, y) {
};

CanvasEngine.CellPoint.prototype.getX = function(){
};

CanvasEngine.CellPoint.prototype.getY = function(){
};

CanvasEngine.CellPoint.prototype.setX = function(x){
};

CanvasEngine.CellPoint.prototype.setY = function(y){
};
