'use strict';

/* global CanvasEngine */

/**
 *
 * @param {number} begin Начальная велична
 * @param {number} end Конечная величина
 * @param {number} speed Конечная величина
 * @param {CanvasEngine.GameCanvas} gameCanvas
 * @constructor
 */
CanvasEngine.AnimationValue = function (begin, end, speed, gameCanvas) {
};

/**
 *
 */
CanvasEngine.AnimationValue.prototype.start = function () {
};

/**
 *
 * @param {number} begin Начальная велична
 * @param {number} end Конечная величина
 */
CanvasEngine.AnimationValue.prototype.setInfo = function (begin, end) {

};

/**
 *
 */
CanvasEngine.AnimationValue.prototype.isPause = function () {
};

/**
 *
 */
CanvasEngine.AnimationValue.prototype.getValue = function () {
};

/**
 *
 * @param {number} direction
 */
CanvasEngine.AnimationValue.prototype.setDirection = function (direction) {
};

/**
 *
 */
CanvasEngine.AnimationValue.prototype.reset = function () {

};