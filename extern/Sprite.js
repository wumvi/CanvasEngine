'use strict';

/* global CanvasEngine */



/**
 * @param {?CanvasEngine.GameCanvas=} opt_gameCanvas = null opt_gameCanvas
 * @constructor
 * @implements {CanvasEngine.SpriteInterface}
 */
CanvasEngine.Sprite = function(opt_gameCanvas) {
};


CanvasEngine.Sprite.prototype.getZIndex = function() {
};


CanvasEngine.Sprite.prototype.setZIndex = function(zIndex) {
};


/**
 *
 * @param {number} speed
 * @return {CanvasEngine.SpeedCoordination}
 */
CanvasEngine.Sprite.prototype.makeSpeedCoordination = function(speed) {
};


CanvasEngine.Sprite.prototype.drawSlide = function(ctx, animKey) {
};


CanvasEngine.Sprite.prototype.drawSlideWithXY = function(ctx, animKey, x, y) {
};


CanvasEngine.Sprite.prototype.drawPartImgWithXY = function(ctx, img, obj, x, y) {
};


/**
 *
 * @param {CanvasEngine.GameCanvas} gameCanvas
 */
CanvasEngine.Sprite.prototype.setGameCanvas = function(gameCanvas) {
};


/**
 *
 * @return {CanvasEngine.GameCanvas|*}
 */
CanvasEngine.Sprite.prototype.getGameCanvas = function() {
};


/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {number} timestamp
 */
CanvasEngine.Sprite.prototype.drawAnimation = function(ctx, timestamp) {
};


/**
 *
 */
CanvasEngine.Sprite.prototype.nextAnimationKey = function() {
};


CanvasEngine.Sprite.prototype.setSpriteInfo = function(imgInfo) {
};


/**
 *
 * @param {Object} imgInfo
 * @param {Object} animationInfo
 */
CanvasEngine.Sprite.prototype.setAnimationInfo = function(imgInfo, animationInfo) {

};


/**
 *
 * @param {CanvasEngine.Coordinates} coordinates
 */
CanvasEngine.Sprite.prototype.linkCoordinates = function(coordinates) {
};


/**
 *
 * @returns {CanvasEngine.Coordinates}
 */
CanvasEngine.Sprite.prototype.getCoordinates = function() {
};


/**
 *
 * @param {CanvasEngine.Coordinates} coordinates
 */
CanvasEngine.Sprite.prototype.setCoordinates = function(coordinates) {
};


/**
 *
 * @param {HTMLCanvasElement|HTMLImageElement} surface
 * @param {string=} key
 *
 * @return {*}
 */
CanvasEngine.Sprite.prototype.setSurface = function(surface, key) {
};


/**
 *
 * @param key
 */
CanvasEngine.Sprite.prototype.setCurrentSurfaceKey = function(key) {
};


/**
 *
 * @param {string=} name
 * @returns {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement}
 */
CanvasEngine.Sprite.prototype.getSurface = function(name) {
};


/**
 *
 * @return {boolean}
 */
CanvasEngine.Sprite.prototype.isDraw = function() {
};


/**
 *
 */
CanvasEngine.Sprite.prototype.hide = function() {
};


/**
 *
 */
CanvasEngine.Sprite.prototype.show = function() {
};


/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {number} timestamp
 */
CanvasEngine.Sprite.prototype.draw = function(ctx, timestamp) {
};


/**
 *
 * @param {HTMLImageElement|HTMLCanvasElement} surface
 * @param {string} name
 */
CanvasEngine.Sprite.prototype.addSurface = function(surface, name) {
};


/**
 *
 * @param {number} width
 */
CanvasEngine.Sprite.prototype.setWidth = function(width) {
};


/**
 *
 * @param height
 */
CanvasEngine.Sprite.prototype.setHeight = function(height) {
};

CanvasEngine.Sprite.prototype.getWidth = function() {
};

CanvasEngine.Sprite.prototype.getHeight = function() {
};

CanvasEngine.Sprite.prototype.getHorizontalCenter = function() {
};

CanvasEngine.Sprite.prototype.getVerticalCenter = function() {
};

CanvasEngine.Sprite.prototype.getVertTop = function() {
};

CanvasEngine.Sprite.prototype.getVertBottom = function() {
};

CanvasEngine.Sprite.prototype.getHorizLeft = function() {
};

CanvasEngine.Sprite.prototype.getHorizRight = function() {
};

CanvasEngine.Sprite.prototype.moveHorizontal = function(speed) {
};

CanvasEngine.Sprite.prototype.moveVertical = function(speed) {
};