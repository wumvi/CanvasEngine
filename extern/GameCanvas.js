'use strict';

/* global CanvasEngine */

/**
 *
 * @param width
 * @param height
 * @constructor
 */
CanvasEngine.GameCanvas = function (width, height) {

};

CanvasEngine.GameCanvas.prototype.sortByZIndex = function () {

};

/**
 *
 * @type {number}
 */
CanvasEngine.GameCanvas.TICKS_PER_SECOND = 30;


/**
 *
 * @param {CanvasEngine.Sprite} item1
 * @param {CanvasEngine.Sprite} item2
 */
CanvasEngine.GameCanvas.prototype.sortByZIndexCallback = function (item1, item2) {
};

/**
 * Add sprite to list
 * @param {CanvasEngine.Sprite} sprite
 */
CanvasEngine.GameCanvas.prototype.addSurface = function (sprite) {
};

CanvasEngine.GameCanvas.prototype.onCameraMove = function (offsetX, offsetY, camera) {
};


CanvasEngine.GameCanvas.prototype.getWidth = function () {
};

CanvasEngine.GameCanvas.prototype.getHeight = function () {
};

CanvasEngine.GameCanvas.prototype.getContext = function () {

};

CanvasEngine.GameCanvas.prototype.isTick = function(){
};

CanvasEngine.GameCanvas.prototype.addTickCallback = function(func){
};

CanvasEngine.GameCanvas.prototype.repaint = function (timestamp) {
};

CanvasEngine.GameCanvas.prototype.getCanvas = function () {
};

CanvasEngine.GameCanvas.prototype.reUpdateFps = function() {
};

CanvasEngine.GameCanvas.prototype.addReUpdateFpsNotifyCallback = function(cb) {
};
