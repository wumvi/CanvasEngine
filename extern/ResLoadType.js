'use strict';

/* global CanvasEngine */

/**
 *
 * @param {string} name
 * @param {Object|CanvasEngine.ResLoadTypeJson} data
 * @constructor
 */
CanvasEngine.ResLoadType = function(name, data) {
};

CanvasEngine.ResLoadType.prototype.getData = function () {
};

/**
 *
 * @returns {string}
 */
CanvasEngine.ResLoadType.prototype.getName = function () {
};