'use strict';

/* global CanvasEngine, Window */

/**
 * Подерживается ли touch screen
 * @returns {boolean} true - если да
 */
CanvasEngine.isTouchDevice = function () {
};


CanvasEngine.getPixelRation = function () {
};

CanvasEngine.getPixelRationPrefix = function (pixelRatio) {
};


CanvasEngine.isAndroidBrowser = function () {
};

CanvasEngine.isNativeAndroidBrowser = function () {
};

CanvasEngine.isIOs = function () {
};

/**
 *
 * @param {Function} cb
 */
CanvasEngine.onScreenRotate = function(cb) {
};

/**
 *
 * @param {function(number):void} callback
 * @param {number} timeout
 */
window.requestAnimationFrameNew = function(callback, timeout){
};

CanvasEngine.DEVICE_PIXEL_RATIO_100 = 1;
//CanvasEngine.DEVICE_PIXEL_RATIO_150 = 1.5;
CanvasEngine.DEVICE_PIXEL_RATIO_200 = 2;
CanvasEngine.DEVICE_PIXEL_RATIO_300 = 3;
CanvasEngine.DEVICE_PIXEL_RATIO_400 = 4;