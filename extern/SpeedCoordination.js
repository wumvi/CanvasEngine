'use strict';

/* global CanvasEngine */


/**
 *
 * @param {number} speed Скорость в пикселах за секунду
 * @param {CanvasEngine.GameCanvas} gameCanvas
 * @constructor
 */
CanvasEngine.SpeedCoordination = function(speed, gameCanvas) {
};

CanvasEngine.SpeedCoordination.prototype.pause = function () {
};

CanvasEngine.SpeedCoordination.prototype.getFullSpeed = function () {
};

CanvasEngine.SpeedCoordination.prototype.isPause = function () {
};

CanvasEngine.SpeedCoordination.prototype.resume = function () {
};

CanvasEngine.SpeedCoordination.prototype.setTimeStamp = function (timestamp) {
};

CanvasEngine.SpeedCoordination.prototype.setSpeedPerSecond = function (speed) {
};

CanvasEngine.SpeedCoordination.prototype.getSpeed = function () {
};